FROM node:12.18.1-alpine as builder
WORKDIR /app
COPY . .
RUN yarn && yarn build
RUN yarn workspaces focus @trieb.work/eci-worker --production
RUN rm -rf worker/src
RUN rm -rf packages/*/src


FROM node:12.18.1-alpine
WORKDIR /app
COPY --from=builder /app .
WORKDIR /app/worker

CMD [ "yarn", "start" ]