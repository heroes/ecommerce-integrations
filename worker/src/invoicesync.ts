/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/no-use-before-define */
import { createConfig, getDocumentBase64StringOrBuffer, getInvoiceById, getInvoices, updateInvoice } from '@trieb.work/zoho-inventory-ts';
import { addFiletoVoucher, createContactIfnotExisting,
    createVoucher,
    getVoucherByVoucherNumber, LexofficeInvoiceObject, LexOfficeVoucherItem, voucherlist } from '@trieb.work/lexoffice-ts';
import dayjs from 'dayjs';
import differenceBy from 'lodash/differenceBy';
import * as Sentry from '@sentry/node';
import { round } from 'reliable-round';
import countries from 'i18n-iso-countries';
import { mapLimit } from 'async';

/**
 * Mark this invoice as successfully transferred to the Accounting Software using custom field.
 * @param invoiceId
 */
const markInvoiceAsTransferred = async (invoiceId :string) => {
    await updateInvoice(invoiceId, { custom_fields: [{ api_name: 'cf_accounting_transferred', value: true }] });
};

export default async () => {
    createConfig({
        zohoClientId: process.env.ZOHO_CLIENT_ID as string,
        zohoClientSecret: process.env.ZOHO_CLIENT_SECRET as string,
        zohoOrgId: process.env.ZOHO_ORGANIZATION_ID as string,
    });
    const olderThan = dayjs().subtract(2, 'day').format('YYYY-MM-DD');
    console.info(`New incoming Invoice Sync Request. Getting a list of invoices older than ${olderThan} to sync from Zoho..`);
    const rawZohoInvoicesList = getInvoices({ date_before: olderThan, status: 'paid', customview_id: '98644000001340007' });
    const rawLexofficeVouchers = voucherlist('salesinvoice', 'open');
    const [LexOfficeVouchers, ZohoInvoices] = await Promise.all([rawLexofficeVouchers, rawZohoInvoicesList]);

    // format the Zoho Invoices. We write the zahlungs ID as an Remark to Lexoffice
    const transformedZohoInvoices = ZohoInvoices.map((invoice) => {
        const returner = {
            type: 'salesinvoice' as 'salesinvoice',
            voucherDate: invoice.date,
            voucherNumber: invoice.invoice_number as string,
            dueDate: invoice.due_date,
            totalGrossAmount: invoice.total,
            remark: `${invoice.reference_number} ${invoice?.custom_field_hash?.cf_zahlungs_id ? invoice.custom_field_hash.cf_zahlungs_id : ''} ${invoice?.custom_field_hash?.cf_paypal_id || ''} ${invoice?.salesorder_number || ''}`,
            zohoId: invoice.invoice_id as string,
        };
        return returner;
    });
    // calculate the difference between Lexoffice existing invoices and Zoho open invoices. We do no longer need this!
    // const toBeCreatedinLexoffice = differenceBy(transformedZohoInvoices, LexOfficeVouchers, 'voucherNumber');
    if (transformedZohoInvoices.length <= 0) console.info('No Invoices to process - accounting is clean');
    mapLimit(transformedZohoInvoices, 3, async (invoice) => {
        const lexOfficeVoucherCheck = await getVoucherByVoucherNumber(invoice.voucherNumber);
        let skipCreation = false;
        let skipFileUpload = false;
        let currentLexofficeVoucherID = '';
        if (lexOfficeVoucherCheck) {
            if (lexOfficeVoucherCheck.totalGrossAmount === invoice.totalGrossAmount) {
                console.log('This invoice is already created in Lexoffice. Skipping creation.');
                skipCreation = true;
                currentLexofficeVoucherID = lexOfficeVoucherCheck.id;
                if (lexOfficeVoucherCheck.files.length > 0) {
                    console.log(`Voucher has File attached as well. Skipping file upload, marking as transferred ${invoice.voucherNumber}`);
                    skipFileUpload = true;
                    await markInvoiceAsTransferred(invoice.zohoId);
                    return true;
                }
            } else {
                const error = new Error(`Invoice ${invoice.voucherNumber} exist in Lexoffice, but with a different amount! This needs be checked manually`);
                console.error(error);
                Sentry.captureException(error);
                return null;
            }
        }

        console.log('This invoice does not yet exist in Lexoffice. Pulling full invoice data from Zoho..');
        const fullZohoInvoice = await getInvoiceById(invoice.zohoId);

        if ((fullZohoInvoice.adjustment) !== 0.00) {
            skipCreation = true;
            skipFileUpload = true;
        }

        // create the line items in the appropriate Format
        let voucherItems :LexOfficeVoucherItem [] = fullZohoInvoice.line_items.map((item) => {
            const gross = grossFromNet(item.item_total, item.tax_percentage);
            if (gross <= 0) throw new Error(`Gross Value <= 0 ! Invoice: ${invoice.zohoId}, line item: ${item}`);
            const lexofficeItem :LexOfficeVoucherItem = {
                amount: gross,
                taxRatePercent: item.tax_percentage,
                taxAmount: round(gross - item.item_total, 2),
                categoryId: '8f8664a8-fd86-11e1-a21f-0800200c9a66',
            };
            return lexofficeItem;
        });
        if (fullZohoInvoice.shipping_charge_inclusive_of_tax > 0) {
            voucherItems.push({
                amount: fullZohoInvoice.shipping_charge_inclusive_of_tax,
                taxAmount: fullZohoInvoice.shipping_charge_tax,
                taxRatePercent: fullZohoInvoice.shipping_charge_tax_percentage || 0,
                categoryId: '8f8664a8-fd86-11e1-a21f-0800200c9a66',
            });
        }

        // Add together all line items gross to calculate a gross discount in the next step
        const itemTotal = round(voucherItems.reduce((acc, current) => acc + current.amount, 0), 2);

        if (fullZohoInvoice.total === (0 || 0.00)) {
            console.log('Zero-Invoice. Skipping creation in Lexoffice');
            skipCreation = true;
            skipFileUpload = true;
            return true;
        }
        let BruttoDiscount = round(itemTotal - fullZohoInvoice.total, 4);

        voucherItems = voucherItems.map((item) => {
            // the applied Discount reduces the item amount to <= 0
            if ((item.amount - BruttoDiscount <= 0)) {
                BruttoDiscount -= item.amount;
                item.amount = 0;
                item.taxAmount = 0;
            // the discount is less than the orderline amount.
            } else {
                item.amount = round(item.amount - BruttoDiscount, 2);
                item.taxAmount = taxAmount(item.amount, item.taxRatePercent);
                BruttoDiscount = 0;
            }
            return item;
        });

        const taxTotal = round(voucherItems.reduce((acc, current) => acc + current.taxAmount, 0), 2);

        const contact = fullZohoInvoice?.contact_persons_details.find((x) => x.email);
        if (!contact || !contact.email) {
            console.error(`The invoice ${fullZohoInvoice.invoice_id} has no contact person or Email Address attached! 
                Please add it or we can't create this invoice in Lexoffice`);
            Sentry.captureException(
                new Error(`The invoice ${fullZohoInvoice.invoice_id} has no contact person or Email Address attached! 
                Please add it or we can't create this invoice in Lexoffice`),
            );
            return null;
        }
        const countryCode = countries.getAlpha2Code(fullZohoInvoice.billing_address.country, 'de');

        if (!skipCreation) {
            try {
                const lexofficeCustomerId = await createContactIfnotExisting({
                    email: contact.email.toLowerCase(),
                    firstName: contact.first_name as string,
                    lastName: contact.last_name as string,
                    street: fullZohoInvoice.billing_address.address,
                    countryCode,
                });

                const createObject :LexofficeInvoiceObject = {
                    ...invoice,
                    voucherStatus: 'paid',
                    totalTaxAmount: taxTotal,
                    taxType: 'gross',
                    useCollectiveContact: false,
                    contactId: lexofficeCustomerId,
                    voucherItems,
                    version: 0,
                };
                console.info(`Creating now ${createObject.voucherNumber} with total gross amount ${createObject.totalGrossAmount} in Lexoffice`);
                const lexofficeInvoiceID = await createVoucher(createObject);
                currentLexofficeVoucherID = lexofficeInvoiceID;
            } catch (error) {
                console.error('Error creating voucher or contact in Lexoffice');
                console.error(error.response?.data);
                console.error(error.response?.config);
                // Sentry.captureException(JSON.stringify(error.response));
                return false;
            }
        }
        if (!skipFileUpload) {
            try {
                console.log('Trying to upload file now..');
                const { rawBuffer, filename } = await getDocumentBase64StringOrBuffer('invoices', fullZohoInvoice.invoice_id);
                const result = (rawBuffer && filename) ? await addFiletoVoucher(rawBuffer, filename, currentLexofficeVoucherID) : null;
                if (result) await markInvoiceAsTransferred(fullZohoInvoice.invoice_id);
                return true;
            } catch (error) {
                console.error('Error uploading file!', error);
                return false;
            }
        }

        return true;
    }, (err) => {
        if (err) console.error(err);
    });
    console.info('------ Process Done ------');

    return true;
};

/**
 * Returns gross value rounded to two decimal points
 * @param net
 * @param taxrate
 */
const grossFromNet = (net:number, taxrate :number) => round(net * (1 + (taxrate / 100)), 2);

const netFormGross = (gross :number, taxrate :number) => round(gross * (1 - (taxrate / 100)), 2);

const taxAmount = (gross :number, taxrate :number) => round(gross / (1 + (taxrate / 100)) * (taxrate / 100), 2);
