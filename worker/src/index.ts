import dotenv from 'dotenv';
import Bull from 'bull';
import invoicesync from './invoicesync';

dotenv.config({ path: '../.env' });

const ENV = process.env.APM_ENV as string;

const redisOpts = {
    host: process.env.REDIS_HOST as string,
    port: parseInt(process.env.REDIS_PORT as string, 10),
    password: process.env.REDIS_PWD as string,
    keyPrefix: `${ENV}_`,
};
const addressImporterQueue = 'addressimporter';
const invoiceSyncQueue = 'invoicesync';

/**
 * Subscribe to different channels on redis
 */
const invoiceWorker = new Bull(invoiceSyncQueue, {
    redis: redisOpts,

});

console.log('Worker started and subscribed to Redis Queue. Environment:', ENV);
invoiceWorker.process(async (job) => {
    console.info('Invoice Job received!', job.data);
    return invoicesync();
});

invoiceWorker.on('error', function(error) {
    // An error occured.
    console.error(error)
  })
