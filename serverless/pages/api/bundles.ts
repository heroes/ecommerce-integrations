import { withSentry } from 'lib/withSentryHOF';
import { NextApiRequest, NextApiResponse } from 'next';
import { createConfig, getBundles, getPackagesTotal } from '@trieb.work/zoho-inventory-ts';
import { validateDate } from '@trieb.work/helpers-ts';

/**
 * Little API route to get bundles and packages by a specific user in a specific time frame. Used for statistics and billing purposes
 * Call with Querystring from, to and createdByName
 */
export default withSentry(async (req :NextApiRequest, res :NextApiResponse) => {
    createConfig({
        zohoClientId: process.env.ZOHO_CLIENT_ID as string,
        zohoClientSecret: process.env.ZOHO_CLIENT_SECRET as string,
        zohoOrgId: process.env.ZOHO_ORGANIZATION_ID as string,
    });
    const from = req.query?.from as string;
    if (!from) return res.status(400).send('From value missing! Add with queryparameter "from"');
    if (!validateDate(from, 'YYYY-MM-DD')) res.status(400).send('From value is not valid!');
    const to = req.query?.to as string;
    if (!to) return res.status(400).send('From value missing! Add with queryparameter "to"');
    if (!validateDate(to, 'YYYY-MM-DD')) res.status(400).send('To value is not valid!');
    const createdByName = req.query?.createdByName as string;

    const result1 = getBundles(from, to, createdByName);
    const result2 = getPackagesTotal({ from, to });
    const [totalBundles, totalPackages] = await Promise.all([result1, result2]);

    return res.json({
        from,
        to,
        totalBundles,
        totalPackages,
    });
});
