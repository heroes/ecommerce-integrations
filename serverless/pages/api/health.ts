import { withSentry } from 'lib/withSentryHOF';
import { NextApiRequest, NextApiResponse } from 'next';

export default withSentry(async (_req :NextApiRequest, res :NextApiResponse) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({
        Status: 'Green',
    }));
});
