import { Tracker } from 'lib/easypost';
import { apmFlush, Logger, withSentry } from 'lib/withSentryHOF';
import { NextApiRequest, NextApiResponse } from 'next';
import * as callZoho from '@trieb.work/zoho-inventory-ts';
import { sendEmail } from 'lib/Mailgun';

/**
 * Incoming Webhooks for shipment updates, for example from easypost trackers.
 */
export default withSentry(async (req :NextApiRequest, res :NextApiResponse) => {
    const logger = Logger(req);
    const easypostBody :Tracker = req.body;

    if (easypostBody.result.status === 'out_for_delivery') {
        const trackingNumber = easypostBody.result.tracking_code;
        logger.info(`The package ${trackingNumber} is out for delivery!`);
        const fullPackageData = await callZoho.getPackageByTrackingNumber(trackingNumber);

        if (!fullPackageData) logger.error(`No package for tracking number ${trackingNumber} found!`);
        // check in the package comments, if we already send out the "out for delivery" email;
        const outForDeliveryEmailComment = fullPackageData?.comments.find((x) => x.description.includes('Out for Delivery Email sent'));

        // Currently we send just tracking updates for STORE and SO orders. Could be changed in the future.
        if (fullPackageData?.salesorder_number?.match(/STORE-|SO-/) && fullPackageData.email && !outForDeliveryEmailComment) {
            // We need to get the customer of the package in order to get the language.
            const customer = await callZoho.getContactById(fullPackageData.customer_id);
            const userLanguage = customer.language_code || 'de';
            const shipmentNotificationsDisabled = customer?.custom_fields.find((x) => x.label === 'Shipment Notifications disabled')?.value;
            if (shipmentNotificationsDisabled) {
                logger.info(`The user ${customer.contact_id} has shipment notifications disabled`);
            } else {
                // we look in the array of contact persons and search for the one assotiated with this package
                const contactPersonEmail = customer.contact_persons.find((x) => x.contact_person_id === fullPackageData.contact_persons[0]).email;
                logger.info(`We send out a Tracking Notification to ${contactPersonEmail}`);
                const trackingPortal = fullPackageData.shipment_order.notes;
                if (!trackingPortal || !trackingPortal.includes('https://')) {
                    logger.error(`Shipment Order Notes with portal URL not found! Package Id: ${fullPackageData.package_id}`);
                    await apmFlush();
                    res.send('success');
                }
                // await callZoho.sendEmailWithTemplate('packages', fullPackageData.package_id, 'Out_for_delivery', fullPackageData.email, userLanguage);
                const { body, subject } = await callZoho.getEmailTemplateData('shipmentorders', fullPackageData.shipment_id, 'Out_for_delivery', userLanguage);
                const bodyWithTracking = body.replace('%TRACKING_PORTAL_URL%', trackingPortal);
                const emailResponse = await sendEmail(contactPersonEmail,
                    subject, bodyWithTracking,
                    'servus@pfefferundfrost.de',
                    'out_for_delivery');
                if (emailResponse) await callZoho.addComment('packages', fullPackageData.package_id, `Out for Delivery Email sent to ${contactPersonEmail}`);
            }
        }
    }

    await apmFlush();
    res.send('success');
});
