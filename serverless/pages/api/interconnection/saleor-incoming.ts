/* eslint-disable no-case-declarations */
/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-console */
import assert from 'assert';
import dayjs from 'dayjs';
import {
    apmFlush,
    Logger,
    withSentry,
} from 'lib/withSentryHOF';
import apm from 'elastic-apm-node';
import { find } from 'lodash';
import isUrl from 'is-valid-http-url';

import * as callMailchimp from 'lib/Mailchimp';
import * as Sentry from '@sentry/node';
import user_transformer from 'lib/saleor2zoho/user_transformer';

import {
    getShopInfo,
    getProduct,
    getOrderQuery,
    paymentTransactionData,
} from 'lib/gql/saleor.gql';
import SaleorClient from 'lib/ApolloSetup';
import { GetOrderQuery, GetOrderQueryVariables, GetPaymentQuery, GetPaymentQueryVariables } from 'gqlTypes/globalTypes';
import { getTransaction } from 'lib/Braintree';
import { NextApiRequest, NextApiResponse } from 'next';
import winston from 'winston';
import { 
    createConfig,
    addAddresstoContact,
    Address,
    Contact,
    ContactPerson,
    ContactWithFullAddresses, createContact,
    createContactPerson, createPayment,
    createSalesorder,
    CustomerPayment, getContact, getContactById,
    getInvoicefromSalesorder, getItembySKU, getSalesorder,
    invoicefromSalesorder, SalesOrder, salesorderConfirm,
    salesOrderEditpage, sendEmailWithTemplate, updateInvoice } from '@trieb.work/zoho-inventory-ts';
import { calculateShippingTaxRate } from '@trieb.work/helpers-ts';

const client = SaleorClient();

const mailchimpActive = process.env.ENABLE_MAILCHIMP === 'true';

let logger :winston.Logger;

export default withSentry(async (req :NextApiRequest, res :NextApiResponse) => {
    logger = Logger(req);
    createConfig({
        zohoClientId: process.env.ZOHO_CLIENT_ID as string,
        zohoClientSecret: process.env.ZOHO_CLIENT_SECRET as string,
        zohoOrgId: process.env.ZOHO_ORGANIZATION_ID as string,
    });
    // Saleor sends the name of the current event in the header
    if (!req.headers['x-saleor-event']) {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        return res.end(JSON.stringify({
            Status: 'Success',
        }));
    }
    const event = req.headers['x-saleor-event'] as string;
    const saleorDomain = req.headers['x-saleor-domain'] as string;

    let { body } = req;
    assert(typeof (body) === 'object');

    if (!body[0]) {
        logger.info('No valid body found. Got this:', body);
        return res.end(JSON.stringify({
            Status: 'Success',
        }));
    }

    body = body[0];

    apm.setLabel('saleor-event', event);
    apm.setLabel('saleor-domain', saleorDomain);

    // drafts in Saleor don't get created in Zoho
    if (body?.status === 'draft' || event === 'fulfillment_created') {
        res.setHeader('Content-Type', 'application/json');
        return res.end(JSON.stringify({
            Status: 'Success',
        }));
    }

    logger.info(`incoming webhook with event ${event}`);

    // we save the payments for an individual call later
    const { payments } :{payments: Payment[]} = body;
    delete body.payments;

    let user_id :string;
    let email :string;
    let shipping_address :SaleorAddress;
    let billing_address :SaleorAddress;
    let zohoShippingAddressId :string;
    let zohoBillingAddressId :string;
    let contact_person_id :string;

    const run = async (event, body) => {
        let salesorder_id: string;
        let salesorder :SalesOrder;
        let customer: { first_name: string; last_name: string; };

        switch (event) {
            case 'customer_created':

                const customerTransformResponse = await addressPreTransform(body, 'de');
                body = customerTransformResponse.body;
                user_id = customerTransformResponse.user_id;
                if (user_id) {
                    logger.info(`The user account already exist. Skipping creation for ${body.contact_persons[0].email}`);
                } else {
                    logger.info('Creating customer account', body.contact_persons[0].email);
                    await createContact(body);
                }

                break;
            case 'order_created':

                // The language of a certain user. Right now, we just set German or English
                const userLang = body?.metadata?.userLang === 'de' ? 'de' : 'en';
                if (!userLang) logger.error('We don\'t know the language of the user! Set it via Order Metadata "userLang"');

                const versandaktion = !!body?.metadata?.versandaktion;
                if (versandaktion) logger.info('This order is a Rohrpost Service Order');
                // if this is an Order coming from a special Landing page / Rohrpost, we do not want to
                // treat this user as individual, but just as a "contact Person" of the the Rohrpost Customer Account
                const rohrpostMainContactId = body?.metadata?.zohoCustomerId;
                if (rohrpostMainContactId) logger.info(`This order gets attached to the Zoho User with Id ${rohrpostMainContactId}`);
                const skuGrusskarte = body?.metadata?.skuGrusskarte;
                if (skuGrusskarte) {
                    logger.info(`We will add a Greeting Card with SKU ${skuGrusskarte} to this order`);
                    body.lines.push({ product_sku: skuGrusskarte, quantity: 1, rate: 0 });
                }

                // the SalesOrder Object that we use to finally send the create Event to Zoho
                const createSalesorderObject :SalesOrder = {};

                shipping_address = body.shipping_address;
                billing_address = body.billing_address;

                const shippingAddressCompany :string = body?.shipping_address?.company_name;
                const billingAddressCompany :string = body?.billing_address?.company_name;
                const transformResponse = await addressPreTransform(body, userLang, rohrpostMainContactId, versandaktion);

                user_id = transformResponse.user_id;
                createSalesorderObject.customer_id = user_id;
                createSalesorderObject.contact_persons = body.contact_persons;
                zohoBillingAddressId = transformResponse.zohoBillingAddressId;
                zohoShippingAddressId = transformResponse.zohoShippingAddressId;
                contact_person_id = transformResponse.contact_person_id;
                const contactPerson = transformResponse.contact_person;
                email = body.user_email;

                // getting more Order Data from Saleor
                const orderDetails = await client.query<GetOrderQuery, GetOrderQueryVariables>({
                    query: getOrderQuery,
                    variables: {
                        id: body.id,
                    },
                });

                const firstName = contactPerson.first_name;
                const lastName = contactPerson.last_name;
                if (!firstName) logger.error('We could not get first and last name. Mailchimp will not know this data!');

                createSalesorderObject.customer_id = user_id;
                const array = [];
                array.push(contact_person_id);
                // body.contact_persons = array;
                createSalesorderObject.contact_persons = array;

                const { zohoPrefixOrderNumber, orderNumber } = salesOrderID(body.id);
                createSalesorderObject.salesorder_number = zohoPrefixOrderNumber;
                createSalesorderObject.reference_number = body.id;

                for (const line in body.lines as WebhookOrderLine) {
                // get the Zoho Item ID and Tax Rate by search for SKU.
                // We take the Tax Rate from Zoho - this is our "truth"
                    const { zohoItemId, zohoTaxRate } = await getItembySKU({
                        product_sku: body.lines[line].product_sku,
                    });
                    body.lines[line].tax_rate = zohoTaxRate;
                    body.lines[line].item_id = zohoItemId;
                    body.lines[line].rate = body.lines[line].unit_price_gross_amount;
                    delete body.lines[line].type;
                    delete body.lines[line].id;
                    delete body.lines[line].product_name;
                }
                createSalesorderObject.line_items = body.lines;
                // body.line_items = body.lines;

                // Saleor is giving us gross prices.
                // body.is_inclusive_tax = true;
                createSalesorderObject.is_inclusive_tax = true;

                // add the delivery method using the name of the shipping zone in saleor
                // body.delivery_method = shippingMethod(body.shipping_method.name);
                createSalesorderObject.delivery_method = shippingMethod(body.shipping_method.name);

                // shipping costs now
                // body.shipping_charge_inclusive_of_tax = body.shipping_method.price_amount;
                createSalesorderObject.shipping_charge_inclusive_of_tax = body.shipping_method.price_amount;

                // set the brutto price for shipping
                // body.shipping_charge = body.shipping_method.price_amount;
                createSalesorderObject.shipping_charge = body.shipping_method.price_amount;

                const taxRate = calculateShippingTaxRate(body.lines, 'tax_rate');
                const { taxes, customFieldReadyToFulfill } = await salesOrderEditpage();
                const tax = taxes.filter((x) => x.tax_percentage === taxRate);
                if (tax.length < 1) throw new Error('The appropriate tax rate is missing in Zoho! Please create it first');

                createSalesorderObject.shipping_charge_tax_id = tax[0].tax_id;
                delete body.shipping_method;
                delete body.lines;

                /**
                 * Create a entity level discount, if we have one
                 */
                if (body.discount_amount) {
                    const orderVoucher = orderDetails.data.order?.voucher;

                    // If we have an entity level percentage discount, we set it also in Zoho.
                    if (orderVoucher.discountValueType === 'PERCENTAGE' && orderVoucher.type === 'ENTIRE_ORDER') {
                        const discountPercentage = orderVoucher.discountValue;
                        createSalesorderObject.discount = `${discountPercentage}%`;
                        createSalesorderObject.is_discount_before_tax = true;
                    }
                    if (orderVoucher.discountValueType === 'FIXED' && orderVoucher.type === 'ENTIRE_ORDER') {
                        createSalesorderObject.discount = orderVoucher.discountValue;
                        createSalesorderObject.is_discount_before_tax = false;
                    }
                    createSalesorderObject.discount_type = 'entity_level';
                }

                // we only mark an order as vorkasse payment, if the right gateway is used and the gross amount is bigger than 0 (could be fully paid by giftcard)
                // if we have a versendeaktion, we do not want to mark the order as Vorkasse Order
                const isVorkassePayment = payments[0]?.gateway === 'triebwork.payments.rechnung' && body.total_gross_amount > 0 && !versandaktion;

                // We set the custom field "ready to fulfill" here. This packages are marked as ready to be send out.
                createSalesorderObject.custom_fields = [{
                    customfield_id: customFieldReadyToFulfill,
                    value: !isVorkassePayment,
                }];

                try {
                // we need to save billing and shipping address
                // but delete it now here, as Zoho just works with adress IDs ..
                    createSalesorderObject.billing_address_id = zohoBillingAddressId;
                    // body.shipping_address_id = zohoShippingAddressId;
                    createSalesorderObject.shipping_address_id = zohoShippingAddressId;

                    // delete body.shipping_address;
                    // delete body.billing_address;
                    const ZohoSalesorder = await createSalesorder(createSalesorderObject, body.total_gross_amount);
                    salesorder_id = ZohoSalesorder.salesorder_id;
                    salesorder = ZohoSalesorder;
                    // confirm the order, when finished. Otherwise it will stay as draft
                    await salesorderConfirm(salesorder_id);
                } catch (error) {
                    if (error?.response?.data?.code === 36004) {
                        logger.info('We already created this salesorder. Proceeding');
                        const result = await getSalesorder(body.salesorder_number);
                        salesorder_id = result.salesorder_id;
                        salesorder = result;
                    } else if (error?.response?.data?.code === 36414) {
                        logger.error('You have exhausted your available sales orders for this month in Zoho. Kindly upgrade your account.');
                        return null;
                    } else {
                        throw new Error(`Error creating Salesorder ${error} ${JSON.stringify(error?.response?.data)} we sent this body: ${JSON.stringify(createSalesorder)}`);
                    }
                }

                // Vorkasse Payment gets send out via email. Zoho tracks, if this salesorder got send emailed before.
                if (isVorkassePayment && payments[0]?.charge_status !== 'fully-charged' && !salesorder.is_emailed) {
                    logger.info('We got a payment with Rechnungs Gateway. Send out Zahlungsdaten');
                    // send out salesorder with vorkasse Email Template and create and invoice draft
                    try {
                        await sendEmailWithTemplate('salesorders', salesorder_id, 'Vorkasse', email, userLang);
                        await invoicefromSalesorder(salesorder_id);
                    } catch (error) {
                        logger.error(error);
                        Sentry.captureException(error);
                    }
                }

                /**
                 * When Mailchimp is enabled, this functionality adds the store, products and orders automatically.
                 */
                if (mailchimpActive) {
                    const { storeData, StoreID } = await setupMailchimp();

                    if (skuGrusskarte) {
                        logger.info('Removing the Grußkarte as Orderline again, as Mailchimp should not know this data');

                        salesorder.line_items = salesorder.line_items.filter((item) => item.sku !== skuGrusskarte);
                    }

                    const mailchimpOrderCheck = await callMailchimp.GetOrder(StoreID, orderNumber);
                    if (mailchimpOrderCheck) {
                        logger.info('This mailchimp order already exists. Skipping any creation...');
                        return true;
                    }

                    const Lines = salesorder.line_items.map((item) => {
                        const returnValue = {
                            id: item.line_item_id,
                            product_id: item.product_id,
                            product_variant_id: item.variant_id,
                            quantity: item.quantity,
                            price: item.rate,
                            discount: item.discount,
                            title: item.name,
                        };

                        return returnValue;
                    });

                    // when we have a versendeaktion, we can't use the main user Id as mailchimp customer id
                    // instead, we use the contact_person_id
                    const mailchimpEcommerceUserId = versandaktion ? contact_person_id : user_id;

                    const mailchimpCustomer = await callMailchimp.GetCustomer(StoreID, mailchimpEcommerceUserId);

                    /**
                    * Set the opt-in status according to metadata in the order! Otherwise they are always just transactional.
                    */
                    const newsletterAccepted = body?.metadata?.newsletterAccepted === 'true';

                    const orderToken = orderDetails.data?.order.token;
                    const domain = process.env.MAIN_DOMAIN;
                    let mailchimpOrderUrl = 'https://pfefferundfrost.de';
                    if (orderToken && domain) {
                        const generatedUrl = generateMailchimpOrderUrl(domain, userLang, orderToken);
                        if (generatedUrl) {
                            mailchimpOrderUrl = generatedUrl;
                        } else {
                            Sentry.captureException(new Error(`Generated a mailchimp order url, that is not valid! ${domain} ${userLang} ${orderToken}`));
                        }
                    }

                    const customerObject :ECommerceCustomer = {
                        id: mailchimpEcommerceUserId,
                    };
                    if (!mailchimpCustomer) {
                        customerObject.email_address = contactPerson.email;
                        customerObject.first_name = contactPerson.first_name;
                        customerObject.last_name = contactPerson.last_name;
                        customerObject.opt_in_status = newsletterAccepted;
                        customerObject.company = body.company_name;
                    } else {
                        logger.info('This user already exist in Mailchimp. We do not need to create it.');
                    }

                    // Mailchimp date Formating. Adding Timezone correctly
                    const createdTime = dayjs(salesorder.created_time).format();

                    const mailchimpOrder :MailchimpOrder = {
                        id: orderNumber,
                        customer: customerObject,
                        currency_code: storeData.data.shop.defaultCurrency,
                        order_total: versandaktion ? 0 : salesorder.total,
                        shipping_total: salesorder.shipping_charge_inclusive_of_tax,
                        tax_total: salesorder.tax_total,
                        processed_at_foreign: createdTime,
                        lines: Lines,
                        shipping_address: {
                            name: `${shipping_address.first_name} ${shipping_address.last_name}`,
                            address1: shipping_address.street_address_1,
                            address2: shipping_address.street_address_2,
                            city: shipping_address.city,
                            postal_code: shipping_address.postal_code,
                            country_code: shipping_address.country,
                            company: shipping_address.company_name,
                        },
                        billing_address: {
                            name: `${billing_address.first_name} ${billing_address.last_name}`,
                            address1: billing_address.street_address_1,
                            address2: billing_address.street_address_2,
                            city: billing_address.city,
                            postal_code: billing_address.postal_code,
                            country_code: billing_address.country,
                            company: billing_address.company_name,
                        },
                        discount_total: versandaktion ? 0 : createSalesorderObject.discount_total,
                        promos: [],
                        landing_site: storeData.data.shop.domain.host,
                        financial_status: 'pending',
                        fulfillment_status: 'pending',
                        order_url: mailchimpOrderUrl,

                    };
                    if (body.discount_name && !versandaktion) {
                        mailchimpOrder.promos.push({ code: body.discount_name, amount_discounted: createSalesorderObject.discount_total, type: 'fixed' });
                    }
                    logger.info('Trying to create the order in Mailchimp now..', mailchimpOrder.id);
                    const CreateResult = await callMailchimp.CreateOrder(StoreID, mailchimpOrder);
                    if (CreateResult.id) logger.info('Order creation in mailchimp successful. ID:', CreateResult.id);

                    const chimpId = callMailchimp.getMailchimpIdFromEmail(email);
                    logger.info(`setting the user language now in Mailchimp for user ${chimpId}, email ${email.toLowerCase()}`);
                    const updateObject = { language: userLang, merge_fields: {} };
                    if (firstName && lastName) {
                        updateObject.merge_fields = {
                            FNAME: firstName,
                            LNAME: lastName,
                        };
                    } else {
                        delete updateObject.merge_fields;
                    }
                    await callMailchimp.UpdateUser(chimpId, updateObject);
                }

                break;

            case 'order_updated':
            // if we have fulfillments
                if (body.fulfillments && body.fulfillments.length > 0) {
                    const fulfillments = body.fulfillments.filter((x) => x.tracking_number);
                    for (const fulfillment of fulfillments) {
                    // Todo: get more data and create the fulfillment in Zoho (if not already existing)

                    }
                }

                break;
            case 'checkout_update':
                logger.info(body);
                break;
            case 'order_fully_paid':
                const ids = salesOrderID(body.id);
                const result = await getSalesorder(ids.zohoPrefixOrderNumber);
                // The order creation is a different API call from Saleor, that might come even after the order fully paid call.
                // we do not want to create the order, when it is not there yet, as this call might interfere with the order creation
                // call from Saleor
                if (!result) {
                    logger.info(`We received a payment for the non-existing salesorder ${ids.zohoPrefixOrderNumber}. Returning 500 and letting Saleor Retry...`);
                    // await run('order_created', body);
                    // result = await getSalesorder(ids.zohoPrefixOrderNumber);
                    // if (!result) throw new Error('Final try to create salesorder was not succesful!');
                    return res.status(500).send('Salesorder does not (yet) exist');
                }
                salesorder_id = result.salesorder_id;
                // create the payment
                if (payments !== null) {
                    let invoice_id :string;
                    let invoice_metadata = { status: '' };

                    // check if we already created an invoice before.
                    if (result.invoiced_status !== 'invoiced') {
                        logger.info('We have to create an Invoice in Zoho now..');
                        try {
                            invoice_id = await invoicefromSalesorder(salesorder_id);
                        } catch (error) {
                            logger.error('Error creating invoice', error);
                            logger.error(error?.data);
                            Sentry.captureException(`${error} ${error?.data}`);
                        }
                        invoice_metadata.status = 'unpaid';
                    } else {
                        logger.info('Invoice does already exist. Getting Invoice ID now ..');
                        const invoiceResult = await getInvoicefromSalesorder(salesorder_id);
                        invoice_id = invoiceResult.invoice_id;
                        invoice_metadata = invoiceResult.metadata;
                    }

                    assert(typeof invoice_id === 'string');

                    const payment = payments[0];
                    const zohoPayment :CustomerPayment = {};

                    // create the payment automatically only when we really charged the customer.
                    if (invoice_metadata.status !== 'paid' && payment.charge_status === 'fully-charged') {
                    // we take the customer id from the corresponding salesorder
                        const { customer_id } = result;

                        zohoPayment.payment_mode = payment.gateway === 'mirumee.payments.braintree' ? 'onlinepayment' : 'banktransfer';
                        zohoPayment.customer_id = customer_id;
                        zohoPayment.amount = payment.captured_amount;
                        zohoPayment.date = dayjs(payment.created).format('YYYY-MM-DD');
                        delete payment.created;
                        zohoPayment.invoices = [{
                            invoice_id,
                            amount_applied: payment.captured_amount,
                        }];
                        logger.info('We set the "Zahlungs-ID" field and create an Payment in Zoho now and add it to invoice', invoice_id);
                        try {
                            const transactionDetails = await client.query<GetPaymentQuery, GetPaymentQueryVariables>({
                                query: paymentTransactionData,
                                variables: {
                                    id: payment.id,
                                },
                            });
                            const { token } = transactionDetails?.data?.payment?.transactions[0];

                            if (token) {
                                const invoiceUpdateObject = {
                                    custom_fields: [{
                                        label: 'Zahlungs-ID',
                                        placeholder: 'cf_zahlungs_id',
                                        value: token,
                                    }],
                                };
                                try {
                                    // get the full transaction data from Braintree. If we have a paypal transaction, we
                                    // set the PayPal ID as well.
                                    const fullBraintreeData = await getTransaction(token);
                                    const payPalTransactionId = fullBraintreeData?.paypalAccount?.authorizationId;
                                    if (payPalTransactionId) {
                                        invoiceUpdateObject.custom_fields.push({
                                            label: 'PayPal-ID',
                                            placeholder: 'cf_paypal_id',
                                            value: payPalTransactionId,
                                        });
                                    }
                                } catch (error) {
                                    logger.error('Error getting Braintree data', error);
                                    Sentry.captureException(new Error(error));
                                }

                                await updateInvoice(invoice_id, invoiceUpdateObject);
                            }
                        } catch (error) {
                            logger.error(error.response?.data ? error.response?.data : error);
                            Sentry.captureException(error?.response?.data ? error.response.data : error);
                        }
                        try {
                            await createPayment(zohoPayment);
                        } catch (error) {
                            if (error?.response?.data?.code === 24016) {
                                logger.info('Payment can not be added. Is most likely already created.');
                            } else {
                                const exception = error?.response?.data || error;
                                logger.error(exception);
                                Sentry.captureException(exception);
                            }
                        }
                        if (mailchimpActive) {
                            try {
                                const { StoreID } = await setupMailchimp();
                                // Make sure that the order is paid in Mailchimp as well
                                logger.info('Setting the order as paid in Mailchimp');
                                const mailchimpOrder = {
                                    financial_status: 'paid',
                                };
                                await callMailchimp.UpdateOrder(StoreID, ids.orderNumber, mailchimpOrder);
                            } catch (error) {
                                logger.error(error);
                                Sentry.captureException(error);
                            }
                        }
                    }
                }
                break;

            case ('product_updated' || 'product_created'):
                if (!mailchimpActive) {
                    logger.info('Mailchimp not activated. Skipping');
                    return true;
                }
                if (!body.is_published) {
                    logger.info('Product is not published. Cant access it right now .. skipping');
                    return true;
                }
                const { StoreID } = await setupMailchimp();
                const productDetails = await client.query({
                    query: getProduct,
                    variables: {
                        id: body.id,
                    },
                });
                logger.info('Finished getting all product details from Saleor for', body.name);
                if (!productDetails.data.product) throw new Error(`no product returned from Saleor! ${JSON.stringify(productDetails.data)} ${body.id}`);
                const { product } = productDetails.data;
                if (product.variants.length < 1) throw new Error(`Getting no variants back from saleor! Skipping ${JSON.stringify(product)}`);
                const returnParallel = product.variants.map(async (variant) => {
                    const { zohoItemId, name } = await getItembySKU({ product_sku: variant.sku });
                    const productUrl = `${process.env.STOREFRONT_PRODUCT_URL}${product.slug}`;
                    const variantImage = variant.images.length > 0 ? variant.images[0].url : product.images[0].url;
                    const title = name;
                    const price = variant.price.amount;
                    const { sku } = variant;
                    const variants = [{
                        id: zohoItemId,
                        image_url: variantImage,
                        title,
                        price,
                        sku,
                    }];
                    const mailchimpProduct = {
                        id: zohoItemId,
                        title,
                        variants,
                        image_url: variantImage,
                        url: productUrl,
                        published_at_foreign: product.updatedAt,
                    };
                    const shopResult = await callMailchimp.GetProduct(StoreID, zohoItemId);
                    if (shopResult) {
                        logger.info('Updating Product', title, 'in Mailchimp..');
                        await callMailchimp.UpdateProduct(StoreID, zohoItemId, mailchimpProduct);
                    } else {
                        logger.info('Creating product', title, 'in Mailchimp..');
                        await callMailchimp.CreateProduct(StoreID, mailchimpProduct);
                    }
                    return true;
                });
                try {
                    await Promise.all(returnParallel);
                } catch (error) {
                    logger.error('Error getting product variant in Zoho');
                    Sentry.captureException(error);
                }

                break;
            default:
                break;
        }
    };
    await run(event, body);

    await apmFlush();

    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    return res.end(JSON.stringify({
        Status: 'Success',
    }));
});

function shippingMethod(name) {
    if (name.includes('DPD')) return 'DPD Germany';
    if (name.includes('DHL')) return 'DHL';
    logger.error('No shipping method in Zoho found that matches the shipping method in Saleor :( we got', name);
}

/**
 * This function formats a saleor base64 encoded Order ID to a valid Zoho Inventory ID like
 *  PREFIX-ORDERNUMBER
 * @param {string} saleorID base64 encoded saleor ID like "T3JkZXI6Nw=="
 */
const salesOrderID = (saleorID :string) => {
    const orderNumber = Buffer.from(saleorID, 'base64').toString('ascii').split(':')[1];
    assert.strictEqual(typeof orderNumber, 'string');
    return { zohoPrefixOrderNumber: `STORE-${orderNumber}`, orderNumber };
};

/**
 * This function sets up Mailchimp Store and returns the needed store ID
 */
const setupMailchimp = async () => {
    const storeData = await client.query({
        query: getShopInfo,
    });
    if (!storeData.data.shop) throw new Error('Did not receive valid result from Saleor to create/update store in MailChimp');
    const StoreID = await callMailchimp.SetupStore(storeData.data.shop);
    return { storeData, StoreID };
};

/**
 * Creates an order URL for customers to get back to their order
 */
const generateMailchimpOrderUrl = (domain :String, language :String, token :string) => {
    const url = `https://${domain}/${language}/checkout/confirmation?orderToken=${token}`;

    return isUrl(url) ? url : false;
};

/**
 * Transforms the body and customer objects to match Zoho and gives you shipping and billing address IDs form Zoho.
 * Creates a user, if it does not exist yet.
 * @param inputBody
 * @param languageCode the language code that will be set when user is created. This will change the language of all E-Mails, that the user receives
 * @param userId Optional: if you set the Zoho User Id already here, we do not need to create the user, but instead use an existing one and just add addresses for example.
 * @param versandService Optional: if set to true, the returning contact person will be from the shipping address
 */
const addressPreTransform = async (inputBody, languageCode :string, userId? :string, versandService? :boolean) => {
    const body = user_transformer(inputBody, versandService);
    let contact_person_id :string = null;
    let zohoBillingAddressId :string = null;
    let zohoShippingAddressId :string = null;
    // search, if the user already exist. When a user for example adds a company name on second order, we will create a new user.
    const searchbody = { ...body.contact_persons[0], company_name: body.company_name };
    logger.info('Looking up user in Zoho ...');
    let user_id :string = userId || await getContact(searchbody);

    // we transformed the user body, so that we just have a contact_person_arry. Here
    // we reconstruct the data for easy access.
    const contactPersonData :ContactPerson = {
        first_name: body.contact_persons[0].first_name,
        last_name: body.contact_persons[0].last_name,
        email: body.contact_persons[0].email,
    };
    if (user_id) {
        logger.info('Customer', body.contact_persons[0].first_name, body.contact_persons[0].last_name, 'with user ID', user_id, 'already exist in Zoho.');
        const contact_details = await getContactById(user_id);
        // contact_person_id = contact_details.contact_persons[0].contact_person_id;
        contact_person_id = await contactPersonId(contact_details,
            { firstName: contactPersonData.first_name, lastName: contactPersonData.last_name, email: contactPersonData.email });

        body.contact_persons[0].contact_person_id = contact_person_id;
        const { billingAddressId, shippingAddressId } = await billingAndShippingAddressId(user_id, contact_details.addresses, body.shipping_address, body.billing_address);
        zohoBillingAddressId = billingAddressId;
        zohoShippingAddressId = shippingAddressId;
    }
    if (!user_id) {
        logger.info('The customer', body.contact_persons[0], 'does not exist in Zoho. We need to create it now');
        const customer_sub_type = body.company_name ? 'business' : 'individual';
        const new_user = {
            contact_name: body.contact_name,
            contact_persons: body.contact_persons,
            language_code: languageCode,
            shipping_address: body.shipping_address,
            billing_address: body.billing_address,
            company_name: body.company_name,
            customer_sub_type,
        };
        const result = await createContact(new_user);
        user_id = result.contact_id;
        contact_person_id = result.contact_person_id;
        zohoBillingAddressId = result.billing_address_id;
        zohoShippingAddressId = result.shipping_address_id;
    }

    if (!contact_person_id) {
        throw new Error(`Out of some reason, we did not get any contact person Id. Can't proceed. User Id: ${user_id}`);
    }
    contactPersonData.contact_person_id = contact_person_id;
    return { body, user_id, contact_person_id, contact_person: contactPersonData, zohoBillingAddressId, zohoShippingAddressId };
};

/**
 * Returns a valid shipping and billing address Id. Creates a suitable address, if no address could be found.
 */
const billingAndShippingAddressId = async (zohoUserId :string, contactDetailAddresses :Address[], shippingAddress :Address, billingAddress :Address) => {
    let shippingAddressId :string;
    let billingAddressId :string;
    const findValidBillingAddress :Address = find(contactDetailAddresses, billingAddress);
    if (findValidBillingAddress) {
        logger.info(`This contact has a suitable billing address with id ${findValidBillingAddress.address_id}`);
        billingAddressId = findValidBillingAddress.address_id;
    } else {
        logger.info('We have to create this billing address');
        const res = await addAddresstoContact(zohoUserId, billingAddress);
        billingAddressId = res;
    }
    const findValidShippingAddress :Address = find(contactDetailAddresses, shippingAddress);
    if (findValidShippingAddress) {
        logger.info('This contact has a suitable shipping address with id', findValidShippingAddress.address_id);
        shippingAddressId = findValidShippingAddress.address_id;
    } else {
        logger.info('We have to create this shipping address');
        const res = await addAddresstoContact(zohoUserId, shippingAddress);
        shippingAddressId = res;
    }
    return { shippingAddressId, billingAddressId };
};

/**
 * Looks for a suitable contact person of a contact. Creates the contact person, if not existing yet.
 * Returns the contact person Id
 * @param contactDetails the contact Details from a Zoho "getContact" API Call
 * @param {} {}
 */
const contactPersonId = async (contactDetails :Contact|ContactWithFullAddresses, { firstName, lastName, email }:{ firstName :string, lastName:string, email:string}) => {
    // a contact person with same email address can't exist twice
    const existingContactPerson = find(contactDetails.contact_persons, { first_name: firstName, last_name: lastName, email })
        || find(contactDetails.contact_persons, { email });
    if (existingContactPerson) {
        return existingContactPerson.contact_person_id;
    }
    logger.info(`Contact Person ${firstName} ${lastName} has to be created and added to the Zoho contact ${contactDetails.contact_id}`);
    const newContactPersonId = await createContactPerson(contactDetails.contact_id, { first_name: firstName, last_name: lastName, email });
    return newContactPersonId;
};
