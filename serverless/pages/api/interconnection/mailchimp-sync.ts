/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
/* eslint-disable import/no-unresolved */
import { apmFlush, withSentry } from 'lib/withSentryHOF';
import { getShopInfo, GetVouchers } from 'lib/gql/saleor.gql';
import SaleorClient from 'lib/ApolloSetup';
import * as callMailchimp from 'lib/Mailchimp';
import { NextApiResponse } from 'next';
import { mapLimit } from 'async';
import { GetVouchersQuery, GetVouchersQueryVariables } from 'gqlTypes/globalTypes';

const client = SaleorClient();

const typesDict = {
    PERCENTAGE: 'percentage',
    SHIPPING: 'fixed',

};
/**
 * This API route is used to setup and sync a mailchimp store with Saleor / Zoho. Some
 * values like vouchers can't get synchronised with webhooks, so this API route needs to get
 * triggered from time to time.
 */
export default withSentry(async (req, res :NextApiResponse) => {
    console.log('incoming mailchimp sync request.');

    const storeData = await client.query({
        query: getShopInfo,
    });
    if (!storeData.data.shop) throw new Error('Did not receive valid result from Saleor to create/update store in MailChimp');
    const Shop = storeData.data.shop;
    const URL = Shop.domain.url;
    const StoreID = await callMailchimp.SetupStore(Shop);
    console.log('The Mailchimp Store ID is', StoreID);

    const VoucherData = await client.query<GetVouchersQuery, GetVouchersQueryVariables>({
        query: GetVouchers,
        variables: {
            first: 100,
        },
    });
    if (!VoucherData.data.vouchers.edges) throw new Error('Did not receive valid voucher data from saleor.');
    const vouchers = VoucherData.data.vouchers.edges.map((x) => x.node);
    await mapLimit(vouchers, 4, async (voucher) => {
        // eslint-disable-next-line no-multi-assign
        const type = voucher.type === 'SHIPPING' ? 'fixed' : voucher.discountValueType.toLowerCase();
        let target = 'total';
        if (voucher.type === 'SPECIFIC_PRODUCT') {
            target = 'per_item';
        } else if (voucher.type === 'SHIPPING') {
            target = 'shipping';
        }
        await callMailchimp.voucherCreateAndUpdate(StoreID, {
            id: voucher.id,
            code: voucher.code,
            start: voucher.startDate,
            end: voucher.endDate,
            amount: type === 'percentage' ? voucher.discountValue / 100 : voucher.discountValue,
            type,
            description: '',
            target,
            count: voucher.used,
            url: URL,
        });
        return true;
    });

    await apmFlush();

    return res.status(200).send('success');
});
