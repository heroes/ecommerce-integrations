/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-console */
/* eslint-disable no-continue */
/* eslint-disable no-await-in-loop */
/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-restricted-syntax */
/* eslint-disable import/no-unresolved */
import { apmFlush, Logger, withSentry } from 'lib/withSentryHOF';
import * as Sentry from '@sentry/node';
import {
    stockLevelMutation,
    addPrivateMetaMutation,
    productVariant,
} from 'lib/gql/saleor.gql';

import SaleorClient from 'lib/ApolloSetup';
import { NextApiRequest, NextApiResponse } from 'next';
import { sendPaidInvoice } from 'modules/zoho-incoming/paidInvoice';
import { getSaleorWarehouses } from 'modules/zoho-incoming/common';

import { packagesLogic } from 'modules/zoho-incoming/package';
import captureOrderSaleor from 'modules/zoho-incoming/captureOrderSaleor';
import winston from 'winston';
import { createConfig, CustomField, getItem, Invoice, SalesOrder, updateSalesorder } from '@trieb.work/zoho-inventory-ts';

const client = SaleorClient();

let logger :winston.Logger;

export default withSentry(async (req :NextApiRequest, res :NextApiResponse) => {
    logger = Logger(req);
    createConfig({
        zohoClientId: process.env.ZOHO_CLIENT_ID as string,
        zohoClientSecret: process.env.ZOHO_CLIENT_SECRET as string,
        zohoOrgId: process.env.ZOHO_ORGANIZATION_ID as string,
    });
    // authenticate the call first
    if (req?.body?.token !== '1234abc') return res.status(500).send('Unauthorized');
    if (!req.body.JSONString) throw new Error('No JSONString body found');
    const body = JSON.parse(req.body.JSONString);

    const invoice :Invoice = body?.invoice;

    // start the process for salesorders
    if (body.salesorder) {
        const { salesorder } : { salesorder :SalesOrder } = body;
        logger.info('Incoming Webhook from Zoho from type "Salesorder"');
        // We don't do anything on drafts
        if (salesorder.order_status === 'draft') {
            logger.info('Received status "draft" - answering with 200 ok');
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify({
                Status: 'Success',
            }));
        }

        // update the stock level of the products in the corresponding salesorder.
        try {
            await updateStockandMeta(body.salesorder);
        } catch (error) {
            logger.error(error);
            Sentry.setTag('function', 'updateStockandMeta');
            Sentry.captureException(error);
        }

        if (salesorder.shipped_status === 'shipped') {
            logger.info('This Salesorder is fully shipped. Setting custom field "ready to fulfill" to false..');
            try {
                await setSalesorderFulfillStatus(salesorder.salesorder_id, salesorder.custom_fields, false);
            } catch (error) {
                logger.error(error);
                Sentry.captureException(error);
            }
        }

        // we have packages in the Webhook - do stuff with them
        if (body.salesorder.packages) {
            const { packages } = salesorder;

            // start the packages Logic. Fulfill and print, etc..
            await packagesLogic(packages, body.salesorder, logger);
        }
    }

    if (invoice?.status === 'paid') {
        logger.info('Incoming Webhook from Zoho from type "Paid Invoice"', { 'zoho-invoice-id': invoice.invoice_id });
        // Send out the invoice via email only when STORE order and only when this invoice is not yet emailed
        const result1 = (invoice.reference_number?.includes('STORE-') && !invoice.is_emailed) ? sendPaidInvoice(invoice) : true;
        const result2 = captureOrderSaleor(body.invoice, logger);
        await Promise.all([result1, result2]);
    }

    logger.info('Finishing process');
    await apmFlush();

    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    return res.end(JSON.stringify({
        Status: 'Success',
    }));
});

/**
 * Update the stock level per order line item. Fetches needed data from Saleor and Zoho. Updates the metdata in Saleor (adding Zoho item_id)
 * @param {object} body
 */
async function updateStockandMeta(salesorder :SalesOrder) {
    const SaleorWarehouses = await getSaleorWarehouses();

    // for every lineitem of a salesorder we pull more data from Zoho and update Saleor
    for (const item of salesorder.line_items) {
        logger.info('Pulling more precise article data from Zoho now..');
        const zohoProductItemId = item.product_id || item.item_id;
        if (!zohoProductItemId) throw new Error(`Product ID not found in item! Please check the item: ${JSON.stringify(item)}`);
        const result = await getItem({
            product_id: zohoProductItemId,
        });

        const saleorIdResult = await client.query({
            query: productVariant,
            variables: {
                sku: result.sku,
            },
        });
        const saleor_id = saleorIdResult.data?.productVariant?.id;
        if (!saleor_id) {
            logger.info(`No Valid data from Saleor. This product does most likely not exist in Saleor! SKU: ${result.sku}, Saleor Result: ${JSON.stringify(saleorIdResult)}`);
            return null;
        }
        const saleorStock = saleorIdResult.data.productVariant.stocks;
        logger.info(`Updating Stock level for ${result.sku} Saleor ID ${saleor_id}`);

        for (const warehouse of result.warehouses) {
            // we filter the saleor warehouses for the current Warehouse from Zoho
            const temp = SaleorWarehouses.find((x) => x.node.name === warehouse.warehouse_name);
            if (!temp) {
                logger.info(`We couldn't find a matching Warehouse in Saleor for Zoho Warehouse ${warehouse.warehouse_name}`);
                continue;
            }
            const saleorWarehouseId = temp.node.id;
            const saleorQuantityAllocated = saleorStock.find((x) => x.warehouse.id === saleorWarehouseId)?.quantityAllocated || 0;
            const stock = parseInt(warehouse.warehouse_actual_available_for_sale_stock, 10);

            // the actual stock is the stock + the current allocated stock in Saleor .. this is a little workaround
            let actualSaleorStock = stock + parseInt(saleorQuantityAllocated, 10);
            // Stock can't be negative for Saleor
            if (actualSaleorStock < 0) actualSaleorStock = 0;

            const data = await client.mutate({
                mutation: stockLevelMutation,
                variables: {
                    variantId: saleor_id,
                    warehouseId: saleorWarehouseId,
                    quantity: actualSaleorStock,
                },
            });
            if (data.data.productVariantStocksUpdate.bulkStockErrors.length > 0) {
                throw new Error(`Error updating stocks in Saleor ${JSON.stringify(data.data.productVariantStocksUpdate.bulkStockErrors)}`);
            }

            /**
             * Set the ZohoID also at the Saleor Product Metadata.
             */
            const MetadataResult = await client.mutate({
                mutation: addPrivateMetaMutation,
                variables: {
                    id: saleor_id,
                    input: [{ key: 'ZohoInventory.item_id', value: result.item_id }],
                },
            });
            if (!MetadataResult.data || MetadataResult.data.updatePrivateMetadata.metadataErrors.length > 0) {
                throw new Error(`Error updating metadata in Saleor. 
                    Received no answer or this error:${JSON.stringify(MetadataResult.data.updatePrivateMetadata.metadataErrors)}`);
            }

            logger.info('Successfully updated the stock level in Saleor');
        }
    }
    return true;
}

/**
 * Set the custom field ready_to_fulfill to true or false in a salesorder
 * @param salesorderId
 * @param customFields
 * @param status
 */
const setSalesorderFulfillStatus = async (salesorderId :string, customFields :CustomField[], status:boolean) => {
    const customField = customFields.find((x) => x.placeholder === 'cf_ready_to_fulfill');
    if (customField.value === status) {
        logger.info(`Ready to fulfill is already ${status}. Skipping.`);
        return true;
    }
    const customFieldId = customField.customfield_id;
    if (!customFieldId) throw new Error('The custom field "cf_ready_to_fulfill" could not be found at this salesorder! Create it in Zoho first as mandatory field');
    const updateData = {
        custom_fields: [
            {
                customfield_id: customFieldId,
                value: status,
            },
        ],
    };
    try {
        await updateSalesorder(salesorderId, updateData, 3);
    } catch (error) {
        logger.error(error);
        Sentry.captureException(error.error || error);
    }
    return true;
};
