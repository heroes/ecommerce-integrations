/* eslint-disable import/no-unresolved */
import ObjectsToCsv from 'objects-to-csv';
import {
    Logger,
    withSentry,
} from 'lib/withSentryHOF';
import { ProductDataFeed } from 'lib/gql/saleor.gql';
import SaleorClient from 'lib/ApolloSetup';
import md5 from 'md5';
import { NextApiRequest, NextApiResponse } from 'next';
import { ProductDataFeedQuery, ProductDataFeedQueryVariables } from 'gqlTypes/globalTypes';

const client = SaleorClient();
if (!process.env.STOREFRONT_PRODUCT_URL) throw new Error('No Storefront Product URL given. Set it with the env variable STOREFRONT_PRODUCT_URL');
const StorefrontProductUrl = process.env.STOREFRONT_PRODUCT_URL;

// ToDo: add language values - make language of the feed configurable

export default withSentry(async (req :NextApiRequest, res :NextApiResponse) => {
    const logger = Logger(req);
    const FinalData = [];

    logger.info('Creating new product data feed');

    const RawData = await client.query<ProductDataFeedQuery, ProductDataFeedQueryVariables>({
        query: ProductDataFeed,
        variables: {
            first: 10,
        },
    });

    RawData.data.products.edges.map((product) => product.node).map((product) => {
        // we get the brand from a product attribute called brand
        const brand = product.attributes.find((x) => x.attribute.name === 'brand')?.values[0]?.name;
        const googleProductCategory = product.attributes.find((x) => x.attribute.name === 'googleProductCategory')?.values[0]?.name;

        const title = product.seoTitle ? product.seoTitle : product.name;
        const { hasVariants } = product.productType;
        product.variants.map((variant) => {
            const variantImageLink = variant.images.length > 0 ? variant.images[0].url : '';
            const singleProductImageLink = product.images.length > 0 ? product.images[1]?.url : '';
            const ean = hasVariants ? variant.metadata?.find((x) => x.key === 'EAN')?.value : product.metadata?.find((x) => x.key === 'EAN')?.value;
            const FinalProduct = {
                id: variant.sku,
                title: hasVariants ? `${title} (${variant.name})` : title,
                description: product.seoDescription,
                image_link: hasVariants ? variantImageLink : singleProductImageLink,
                link: StorefrontProductUrl + product.slug,
                price: `${variant.pricing.priceUndiscounted.gross.amount} ${variant.pricing.priceUndiscounted.gross.currency}`,
                sale_price: `${variant.pricing.price.gross.amount} ${variant.pricing.price.gross.currency}`,
                condition: 'new',
                gtin: ean,
                brand,
                availability: variant.quantityAvailable < 1 ? 'out of stock' : 'in stock',
                google_product_category: googleProductCategory,
            };
            FinalData.push(FinalProduct);
            return variant;
        });
        return product;
    });

    const csv = new ObjectsToCsv(FinalData);
    const returnValue = await csv.toString();
    const hash = md5(returnValue);

    res.setHeader('Content-Type', 'text/csv');
    res.setHeader('Content-Disposition', `attachment; filename=productdatafeed-${hash}.csv`);
    res.setHeader('Cache-Control', 's-maxage=1, stale-while-revalidate');
    return res.send(returnValue);
});
