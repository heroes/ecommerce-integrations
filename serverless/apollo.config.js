module.exports = {
    client: {
        includes: [
            'lib/gql/*.gql.tsx'
        ],
        service: {
            name: 'saleor-dev',
            url: 'https://pundf-test-api.triebwork.com/graphql/',
            // optional headers
            headers: {
                authorization: 'Bearer uydAcTsSEgudPeB1bkqlH4NNV093Y0',
            },
            // optional disable SSL validation check
            skipSSLValidation: true,
        },
    },
};
