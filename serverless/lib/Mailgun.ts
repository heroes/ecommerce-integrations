import { NodeMailgun } from 'ts-mailgun';

const mailer = new NodeMailgun();

const setup = () => {
    mailer.apiKey = process.env.MAILGUN_API_KEY;
    mailer.fromTitle = 'Pfeffer & Frost';
    mailer.domain = process.env.MAILGUN_DOMAIN;
    mailer.fromEmail = process.env.MAILGUN_FROM_EMAIL;
    mailer.options = {
        host: 'api.eu.mailgun.net',
    };

    mailer.init();

    return mailer;
};
export const sendEmail = async (toEmail :string, subject :string, body :string, replyTo:string, tags? :string) => {
    const mailgun = setup();
    const additionals = {};
    if (replyTo) additionals['h:Reply-To'] = replyTo;
    // if (tags) additionals['o:tag'] = tags;

    await mailgun.send(toEmail, subject, body, {}, additionals);
    return true;
};
