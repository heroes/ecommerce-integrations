import braintree from 'braintree';

const dev = process.env.APM_ENV === 'dev';

const merchantId = process.env.BRAINTREE_MERCHANT_ID;
const publicKey = process.env.BRAINTREE_PUBLIC_KEY;
const privateKey = process.env.BRAINTREE_PRIVATE_KEY;

const setup = () => {
    const gateway = new braintree.BraintreeGateway({
        environment: dev ? braintree.Environment.Sandbox : braintree.Environment.Production,
        merchantId,
        publicKey,
        privateKey,
    });
    return gateway;
};

export const getTransaction = async (transactionId :string) => {
    const gateway = setup();
    const details = await gateway.transaction.find(transactionId);

    return details as braintree.Transaction;
};
