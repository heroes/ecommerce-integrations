/* eslint-disable no-use-before-define */
/* eslint-disable object-curly-newline */
import Mailchimp from 'mailchimp-api-v3';
import assert from 'assert';
import { encode } from 'js-base64';
import crypto from 'crypto';
import { map } from 'async';

const listId = process.env.MAILCHIMP_LIST_ID;

/**
 * This function checks if the store is already registered in Mailchimp
 * and creates it if not.
 * @param {object} param0
 * @param {string} param0.name The store name. This name gets rendered in Emails and at the Mailchimp website
 * @param {string} param0.defaultCurrency The default currency as ISO string
 * @param {object} param0.companyAddress The company address object
 * @param {object} param0.domain
 */
export async function SetupStore({
    name,
    defaultCurrency,
    companyAddress,
    domain,
} : { name:string, defaultCurrency :string,
    companyAddress?: {streetAddress1 :string, streetAddress2:string, city:string,
        postalCode:string,
        country :{code :string} }, domain:{host:string} }) {
    if (!domain.host) throw new Error('No valid Saleor Store Host set. Please set in the Saleor settings page.');
    if (!listId) throw new Error('No mailchimp listId found. Set it via env variable MAILCHIMP_listId');
    if (!companyAddress) console.error(`No Store Company Adress given! Set it in Saleor. We got name: ${name}`);

    const mailchimp = new Mailchimp(process.env.MAILCHIMP_API_KEY);

    /*
     * We use the store host value as individual ID in mailchimp
     */
    const mailchimpStoreId = encode(domain.host);
    const result = await mailchimp.get({
        path: `/ecommerce/stores/${mailchimpStoreId}`,
    }).catch((error) => {
        if (error.statusCode === 404) {
            console.log('No store yet');
        } else {
            throw new Error(`Error getting mailchimp store, ${error}`);
        }
    });
    if (!result) {
        console.log('The Mailchimp store', domain.host, 'does not exist. We create it now.');
        const Create = await mailchimp.post({
            path: '/ecommerce/stores',
            body: {
                id: mailchimpStoreId,
                list_id: listId,
                name,
                platform: 'Saleor',
                currency_code: defaultCurrency,
                domain: domain.host,
                address: {
                    address1: companyAddress.streetAddress1,
                    address2: companyAddress.streetAddress2,
                    city: companyAddress.city,
                    postal_code: companyAddress.postalCode,
                    country_code: companyAddress.country.code,
                },
            },
        });
        assert.strictEqual(Create.id, mailchimpStoreId);
    }
    const mergeFields = [{
        name: 'Package Tracking Number',
        type: 'text',
        tag: 'MMERGE45',
        required: false,
        public: false,
    },
    {
        name: 'Package Tracking Provider',
        type: 'text',
        tag: 'MMERGE46',
        required: false,
        public: false,
    },
    {
        name: 'Package Tracking URL',
        type: 'url',
        tag: 'MMERGE47',
        required: false,
        public: false,
    },
    {
        name: 'Invoice Download Link',
        type: 'url',
        tag: 'MMERGE48',
        required: false,
        public: false,
    },
    {
        name: 'Invoice Number',
        type: 'text',
        tag: 'MMERGE49',
        required: false,
        public: false,
    },
    {
        name: 'Order Number',
        type: 'text',
        tag: 'MMERGE50',
        required: false,
        public: false,
    },
    ];
    console.log('Verifying merge fields in mailchimp ..');
    map(mergeFields, async (field) => {
        try {
            await mailchimp.post({
                path: `/lists/${listId}/merge-fields`,
                body: field,
            });
        } catch (error) {
            if (error.statusCode !== 400) {
                throw new Error(`Error creating merge fields in Mailchimp!${JSON.stringify(error)}`);
            }
        }
    });

    return mailchimpStoreId;
}

/**
 * returns the MD5 hash of the lowercase Email address, to access that person
 */
export const getMailchimpIdFromEmail = (email :string) => crypto.createHash('md5').update(email.toLowerCase()).digest('hex');

/**
 *
 * @param {string} StoreId The ID you choose when creating the store
 * @param {object} Order The Order details in the official mailchimp format.
 */
export async function CreateOrder(StoreId, Order :MailchimpOrder) {
    const mailchimp = new Mailchimp(process.env.MAILCHIMP_API_KEY);

    try {
        const checkExistence = await mailchimp.get({
            path: `/ecommerce/stores/${StoreId}/orders/${Order.id}`,
        });
        if (checkExistence) {
            return Order.id;
        }
    } catch (error) {
        console.log('Order does not yet exist in Mailchimp');
    }
    const Create = await mailchimp.post({
        path: `/ecommerce/stores/${StoreId}/orders/`,
        body: Order,
    });

    return Create;
}

export async function GetProduct(StoreId, ProductID) {
    const mailchimp = new Mailchimp(process.env.MAILCHIMP_API_KEY);

    const result = await mailchimp.get({
        path: `/ecommerce/stores/${StoreId}/products/${ProductID}`,
    }).catch((error) => {
        if (error.statusCode !== 404) {
            throw new Error(`Error getting mailchimp order, ${error}`);
        }
        return null;
    });

    return result;
}

/**
 * Gets an order from Mailchimp. If not existing, returns null
 * @param StoreId
 * @param OrderID
 */
export async function GetOrder(StoreId, OrderID) {
    const mailchimp = new Mailchimp(process.env.MAILCHIMP_API_KEY);

    const result = await mailchimp.get({
        path: `/ecommerce/stores/${StoreId}/orders/${OrderID}`,
    }).catch((error) => {
        if (error.statusCode !== 404) {
            throw new Error(`Error getting mailchimp order, ${error}`);
        }
        return null;
    });

    return result;
}

export async function CreateProduct(StoreId, Product) {
    const mailchimp = new Mailchimp(process.env.MAILCHIMP_API_KEY);

    const Create = await mailchimp.post({
        path: `/ecommerce/stores/${StoreId}/products`,
        body: Product,
    });

    assert.strictEqual(Create.statusCode, 200);
    console.error('Created new Mailchimp product', Create.title);

    return Product.id;
}

export async function UpdateProduct(StoreId, ProductId, data) {
    const mailchimp = new Mailchimp(process.env.MAILCHIMP_API_KEY);

    const Update = await mailchimp.patch({
        path: `/ecommerce/stores/${StoreId}/products/${ProductId}`,
        body: data,
    });
    assert.strictEqual(Update.statusCode, 200);
}

export async function addMemberToList(email :string, status :MailchimpMemberStatus) {
    const mailchimp = new Mailchimp(process.env.MAILCHIMP_API_KEY);

    const create = await mailchimp.post({
        path: `/lists/${listId}/members`,
        body: {
            email_address: email,
            status,
        },
    });
}

/**
 * Updates a specific Mailchimp User. Get the user ID by creating an md5sum of the email address.
 * @param {string} md5sum
 * @param {object} data
 */
export async function UpdateUser(md5sum, data) {
    const mailchimp = new Mailchimp(process.env.MAILCHIMP_API_KEY);

    const Update = await mailchimp.patch({
        path: `/lists/${listId}/members/${md5sum}`,
        body: data,
    });
    assert.strictEqual(Update.statusCode, 200);
}

/**
 * Update the details of a specific order
 * @param {string} StoreId
 * @param {string} OrderId
 * @param {object} data
 */
export async function UpdateOrder(StoreId, OrderId, data) {
    const mailchimp = new Mailchimp(process.env.MAILCHIMP_API_KEY);

    const Update = await mailchimp.patch({
        path: `/ecommerce/stores/${StoreId}/orders/${OrderId}`,
        body: data,
    });
    assert.strictEqual(Update.statusCode, 200);
}

/**
 * Creates and updates promo rules and codes in Mailchimp.
 * @param {*} StoreId the store id / url
 * @param {*} param1 the voucher object
 * @param {string} param1.id the voucher and promo rule id (we use the same for both)
 * @param {string} param1.code the voucher code
 * @param {string} param1.start the beginning of the validity period
 * @param {string} param1.end the end of the validity period
 * @param {string} param1.type can be fixed or percentage. For free shipping set to fixed
 * @param {string} param1.target per_item, total, shipping
 * @param {integer} param1.count the usage count
 */
export async function voucherCreateAndUpdate(StoreId, { id, code, start, end, type, amount, target, description, count, url }) {
    const mailchimp = new Mailchimp(process.env.MAILCHIMP_API_KEY);

    const checkExistence = await mailchimp.get({
        path: `/ecommerce/stores/${StoreId}/promo-rules/${id}`,
    }).catch((error) => {
        if (error.statusCode === 404) {
            console.log('Promo Rule has to be created');
        } else {
            throw new Error('Error trying to get Promo Rule');
        }
    });
    const body = {
        id,
        starts_at: start,
        ends_at: end,
        type,
        amount,
        target,
        description,
    };
    removeEmpty(body);

    // when promo rule already exist, we patch, otherwise we create it.
    if (checkExistence) {
        await mailchimp.patch({
            path: `/ecommerce/stores/${StoreId}/promo-rules/${id}`,
            body,
        }).catch((error) => {
            console.error(error);
        });
    } else {
        await mailchimp.post({
            path: `/ecommerce/stores/${StoreId}/promo-rules`,
            body,
        }).catch((error) => {
            console.error(error);
        });
    }

    // check if the code is already existing, else create it
    const checkCodeExistence = await mailchimp.get({
        path: `/ecommerce/stores/${StoreId}/promo-rules/${id}/promo-codes/${id}`,
    }).catch((error) => {
        if (error.statusCode === 404) {
            console.log('Promo Code has to be created');
        } else {
            throw new Error('Error trying to get Promo Code');
        }
    });
    type CodeBody = {
        id?:string,
        code :string,
        usage_count:number,
        redemption_url:string,
    };
    const codebody :CodeBody = {
        code,
        usage_count: count,
        redemption_url: url,
    };
    if (checkCodeExistence) {
        await mailchimp.patch({
            path: `/ecommerce/stores/${StoreId}/promo-rules/${id}/promo-codes/${id}`,
            body: codebody,
        });
    } else {
        codebody.id = id;
        await mailchimp.post({
            path: `/ecommerce/stores/${StoreId}/promo-rules/${id}/promo-codes`,
            body: codebody,
        });
    }
}

/**
 * Trigger an Event for a specific user. The event with this name must be created in Mailchimp first.
 * @param memberHash
 * @param eventName
 */
export const triggerEvent = async (memberHash :string, eventName :string) => {
    const mailchimp = new Mailchimp(process.env.MAILCHIMP_API_KEY);
    const chimpUser = await mailchimp.get({
        path: `/lists/${listId}/members/${memberHash}`,
    });
    const currentStatus = chimpUser.status;
    let changedUser = false;
    if (currentStatus !== 'subscribed') {
        console.log('Mailchimp user is only transactional. Changing the Mailchimp user to "subscribed" to send the Invoice E-Mail. Changing back afterwards.');
        await UpdateUser(memberHash, { status: 'subscribed' });
        await new Promise((resolve) => setTimeout(resolve, 3000));
        changedUser = true;
    }
    const result = await mailchimp.post({
        path: `/lists/${listId}/members/${memberHash}/events`,
        body: {
            name: eventName,
        },
    });
    assert.strictEqual(result.statusCode, 204);

    if (changedUser) {
        await UpdateUser(memberHash, { status: currentStatus });
    }
    return true;
};

/**
 * Get a specific ecommerce customer from Mailchimp. Checks, if customer already exist.
 * @param StoreId
 * @param customerID
 */
export const GetCustomer = async (StoreId :string, customerID :string) => {
    const mailchimp = new Mailchimp(process.env.MAILCHIMP_API_KEY);
    const user = await mailchimp.get({
        path: `/ecommerce/stores/${StoreId}/customers/${customerID}`,
    }).catch((e) => {
        if (e.statusCode === 404) {
            console.log('This user could not be found in mailchimp');
            return false;
        }
    });
    return user;
};
/**
 * Just recursively clean an object. Delete all empty values.
 * @param obj
 */
const removeEmpty = (obj) => {
    Object.keys(obj).forEach((key) => {
        if (obj[key] && typeof obj[key] === 'object') removeEmpty(obj[key]);
        else if (obj[key] === (undefined || null)) delete obj[key];
    });
    return obj;
};
