import * as Sentry from '@sentry/node';
import { NextApiHandler, NextApiRequest, NextApiResponse } from 'next';
import apm from 'elastic-apm-node';
import winston from 'winston';

import { ElasticsearchTransport, ElasticsearchTransportOptions } from 'winston-elasticsearch';

const { NODE_ENV, ELASTIC_SERVER } = process.env;
const elasticServer = ELASTIC_SERVER || 'http://localhost:808';

const setup = () => {
    if (apm.isStarted()) return apm;
    const returnApm = apm.start({
        serviceName: 'eCommerce-Integrations',
        environment: process.env.APM_ENV || 'dev',
        serverUrl: process.env.APM_SERVER,
        secretToken: process.env.APM_SECRET_TOKEN,
        usePathAsTransactionName: true,
        metricsInterval: '3s',
        apiRequestTime: '5s',
        captureBody: 'transactions',
        active: process.env.NODE_ENV === 'production',
    });
    return returnApm;
};
setup();

const esTransportOpts :ElasticsearchTransportOptions = {
    apm: setup(),
    clientOpts: { node: elasticServer,
        auth: {
            username: 'logger',
            password: 'logger',
        } },
};
const esTransport = new ElasticsearchTransport(esTransportOpts);

type MetaObject = {
    'saleor-domain'?: string,
    'saleor-event'?: string,
    'zoho-org-id'?: string,
    'easypost-event-id'?: string,
    'easypost-user-id'?: string,
};

/**
 * Creates and Returns the Standard Winston logger with metadata
 * @param req The next.js Req Object
 */
export const Logger = (req? :NextApiRequest) => {
    const defaultMeta :MetaObject = {};
    defaultMeta['saleor-domain'] = req?.headers?.['x-saleor-domain'] as string;
    defaultMeta['saleor-event'] = req?.headers?.['x-saleor-event'] as string;
    defaultMeta['easypost-event-id'] = req?.body?.id as string;
    defaultMeta['easypost-user-id'] = req?.headers?.['X-Webhook-User-Id'] as string;
    defaultMeta['zoho-org-id'] = req?.headers?.['x-com-zoho-organizationid'] as string || req?.query?.['zoho-org-id'] as string;
    const winstonLogger = winston.createLogger({
        level: 'info',
        defaultMeta,
        transports: [
            esTransport,
        ],
    });
    winstonLogger.on('error', (error) => {
        console.error('Error caught', error);
    });
    esTransport.on('error', (error) => {
        console.error('ES transport error', error);
    });
    winstonLogger.add(new winston.transports.Console({
        format: winston.format.simple(),
    }));
    // overwriting all console.logs
    // console.log = (...args) => winstonLogger.info.call(winstonLogger, ...args);

    return winstonLogger;
};

export const logger = Logger();
// console.info = (...args) => logger.info.call(logger, ...args);
// console.warn = (...args) => logger.warn.call(logger, ...args);
// console.error = (...args) => logger.error.call(logger, ...args);

Sentry.init({
    dsn: process.env.SENTRY_DSN,
    environment: process.env.APM_ENV,
});

/**
 * Flush logs and APM data to elasticsearch before ending the session
 */
export const apmFlush = async () => {
    setup();
    const res1 = esTransport.flush();
    const res2 = new Promise((resolve, reject) => {
        if (NODE_ENV !== 'production') return resolve();
        apm.flush((err: any) => {
            if (err) return reject(err);
            return resolve();
        });
    });
    await Promise.all([res1, res2]);
    return true;
};

export const withSentry = (apiHandler :NextApiHandler) => {
    if (NODE_ENV === 'production') {
        return async (req :NextApiRequest, res :NextApiResponse) => {
            try {
                return await apiHandler(req, res);
            } catch (error) {
                Logger().error(error);
                Sentry.captureException(error);
                apm.captureError(error);
                const apmPromise = apmFlush();
                const sentryPromise = Sentry.flush(2000);
                await Promise.all([apmPromise, sentryPromise]);
                return error;
            }
        };
    }
    return async (req :NextApiRequest, res :NextApiResponse) => apiHandler(req, res);
};
