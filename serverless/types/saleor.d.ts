type Payment = {
    'type': 'Payment',
    'id': string,
    'gateway':string,
    'is_active': true,
    'created': '2020-04-15T16:42:39.130Z',
    'modified': '2020-04-15T16:42:39.130Z',
    'charge_status': 'fully-charged',
    'total': '22.91',
    'captured_amount': '22.91',
    'currency': 'EUR',
    'billing_email': 'zinkljannik@gmail.com',
    'billing_first_name': 'Jannik',
    'billing_last_name': 'Zinkl',
    'billing_company_name': '',
    'billing_address_1': 'Humboldtstra\u00dfe 101',
    'billing_address_2': '',
    'billing_city': 'N\u00dcRNBERG',
    'billing_city_area': '',
    'billing_postal_code': '90459',
    'billing_country_code': 'DE',
    'billing_country_area': ''
};
type WebhookOrderLine = {

    'type': 'OrderLine',
    'id': string,
    'product_name': 'Designerdose Charlotte Dumortier',
    'variant_name': 'Gemischt',
    'translated_product_name': '',
    'translated_variant_name': '',
    'product_sku': 'designerdose_charlotte_dumortier_sorte_gemischt',
    'quantity': 1,
    'currency': 'EUR',
    'unit_price_net_amount': '21.90',
    'unit_price_gross_amount': '21.90',
    'tax_rate': '0.00'

};
type SaleorAddress = {
    'type': 'Address',
    'id': string,
    'first_name': string,
    'last_name': 'Zinkl',
    'company_name': '',
    'street_address_1': string,
    'street_address_2': string,
    'city': 'N\u00dcRNBERG',
    'city_area': '',
    'postal_code': string,
    'country': 'DE',
    'country_area': '',
    'phone': ''
};
