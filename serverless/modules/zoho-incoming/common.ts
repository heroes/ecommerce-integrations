import { warehouseQuery } from 'lib/gql/saleor.gql';
import SaleorClient from 'lib/ApolloSetup';
import { GetWarehousesQuery, GetWarehousesQueryVariables } from 'gqlTypes/globalTypes';
import logger from 'winston';

const client = SaleorClient();

/**
 * The Mailchimp Order ID is dynamic - when used with a order coming from Saleor, we want to use this order number. Otherwise we use the Zoho Order ID
 * @param zohoSalesorderNumber
 */
export const returnMailchimpOrderId = (zohoSalesorderNumber :string) => {
    if (zohoSalesorderNumber.match(/STORE-\d+/)) {
        const mailchimpid = zohoSalesorderNumber.split('-')[1];
        return mailchimpid;
    }
    logger.info('This is a non-saleor order ID. Returning the incomig Zoho Salesorder Number');
    return zohoSalesorderNumber;
};

/**
 * Loads all warehouses (first 20) from Saleor and returns them as array.
 */
export async function getSaleorWarehouses() {
    // we pull all warehouse data from Saleor to match them later with Zoho Warehouses
    const data = await client.query<GetWarehousesQuery, GetWarehousesQueryVariables>({
        query: warehouseQuery,
        variables: {
            first: 20,
        },
    });
    const SaleorWarehouses = data.data.warehouses.edges;
    if (SaleorWarehouses.length < 1) throw new Error('No warehouses in Saleor found. Please create them first');

    return SaleorWarehouses;
}
