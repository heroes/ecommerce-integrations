import { getContactById, Invoice, sendEmailWithTemplate } from '@trieb.work/zoho-inventory-ts';
import logger from 'winston';

/**
 * Setting an order as paid in Mailchimp. Generating Invoice Download Link and send out invoice
 * @param invoice The Zoho Invoice Object
 */
export const sendPaidInvoice = async (invoice :Invoice) => {
    const email = invoice?.contact_persons_details[0]?.email;
    if (!email) {
        logger.error('We got an invoice without contact person assotiated. We can\'t send this invoice out automatically!', { 'zoho-invoice-id': invoice.invoice_id });
        return true;
    }
    if (invoice.total <= 0) {
        logger.info('This is a zero invoice. We most likely dont want to send it out', { 'zoho-invoice-id': invoice.invoice_id });
        return true;
    }
    const user = await getContactById(invoice.customer_id, false);
    const userLang = user.language_code;
    await sendEmailWithTemplate('invoices', invoice.invoice_id, 'Paid_Invoice', email, userLang);

    return true;
};
