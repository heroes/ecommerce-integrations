/* eslint-disable no-continue */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
/* eslint-disable @typescript-eslint/no-loop-func */
import { addNote, getContactById, getPackage, getSalesorderById, Package, SalesOrder } from '@trieb.work/zoho-inventory-ts';
import * as Sentry from '@sentry/node';
import { addTracker } from 'lib/easypost';
import * as callMailchimp from 'lib/Mailchimp';
import {
    fulfillmentMutation,
    getOrderQuery,
    getShopInfo,
    updateTrackingMutation,
} from 'lib/gql/saleor.gql';
import SaleorClient from 'lib/ApolloSetup';
import { getSaleorWarehouses, returnMailchimpOrderId } from 'modules/zoho-incoming/common';
import assert from 'assert';
import {
    CreateFulfillmentMutation,
    CreateFulfillmentMutationVariables,
    GetOrderQuery, GetOrderQueryVariables,
    OrderFulfillLineInput, SetTrackingNumberMutation,
    SetTrackingNumberMutationVariables, ShopInfoQuery, ShopInfoQueryVariables } from 'gqlTypes/globalTypes';
import winston from 'winston';
import { printCarrierLabel, printCustomsDocuments, printPackageSlip } from './printingHelpers';

const client = SaleorClient();

const mailchimpActive = process.env.ENABLE_MAILCHIMP === 'true';

type Carrier = 'DPD';
type CountryCode = 'de' | 'ch' |'at' | 'es' | 'uk' | 'nl';
/**
 * Generates a tracking portal URL for different carriers and countries.
 * @param carrier
 * @param countryCode
 * @param trackingCode
 * @param zip
 */
export const generateTrackingPortalURL = (carrier :Carrier, countryCode :CountryCode, trackingCode :string, zip :string) => {
    const patterns = {
        de: {
            DPD: `https://my.dpd.de/redirect.aspx?action=1&pno=${trackingCode}&zip=${zip}&lng=De_de`,
        },
        ch: {
            DPD: `https://www.dpdgroup.com/ch/mydpd/tmp/basicsearch?parcel_id=${trackingCode}`,
        },
        at: {
            DPD: `https://www.mydpd.at/meine-pakete?pno=${trackingCode}`,
        },
        es: {
            DPD: `https://www.seur.com/livetracking/?segOnlineIdentificador=${trackingCode}&segOnlineIdioma=es`,
        },
        fr: {
            DPD: `https://www.dpd.fr/trace/${trackingCode}`,
        },
        no: {
            DPD: `https://tracking.postnord.com/no/?id=${trackingCode}`,
        },
        uk: {
            DPD: `https://apis.track.dpd.co.uk/v1/track?parcel=${trackingCode}`,
        },
        be: {
            DPD: `https://www.dpdgroup.com/be/mydpd/tmp/basicsearch?parcel_id=${trackingCode}`,
        },
        nl: {
            DPD: `https://tracking.dpd.de/status/nl_NL/parcel/${trackingCode}`,
        },
    };

    const returnUrl = countryCode ? patterns[countryCode][carrier] : null || null;

    return returnUrl;
};

/**
 * Create a fulfillment with tracking number in Saleor.
 * @param param0
 */
const fulfillOrderInSaleor = async ({ SaleorOrder, packagedata, reference_number }:{ packagedata :Package, reference_number :string, SaleorOrder :GetOrderQuery['order']}, 
    logger :winston.Logger) => {
    if (SaleorOrder.fulfillments.find((full) => full.trackingNumber.includes(packagedata.tracking_number))) {
        logger.info('Tracking number already found in this order. Skipping fulfillment create. Tracking number:', packagedata.tracking_number);
        return null;
    }

    const SaleorWarehouses = await getSaleorWarehouses();

    // go through all line items of the Zoho Package and change the data to map to Saleor Format
    // In Zoho it can happen, that you have two orderlines with the same product - in saleor this
    // can't happen
    const lines :OrderFulfillLineInput[] = [];
    for (const lineItem of packagedata.line_items) {
        // get the orderlineID with the SKU
        const orderLineId = SaleorOrder.lines.find((x) => x.productSku === lineItem.sku)?.id;
        if (!orderLineId) {
            logger.info(`Couldn't find this SKU in any orderline in Saleor ${lineItem.sku}. \
            This orderline could be added manually in Zoho. Order Number: ${packagedata.salesorder_number}`);
            continue;
        }

        const temp = SaleorWarehouses.find((x) => x.node.name === lineItem.warehouse_name);
        if (!temp) {
            logger.info('We couldn\'t find a matching Warehouse in Saleor for Zoho Warehouse', lineItem.warehouse_name);
            continue;
        }
        const stocks = [{
            warehouse: temp.node.id,
            quantity: lineItem.quantity,
        }];

        lines.push({
            orderLineId,
            stocks,
        });
    }
    assert(typeof reference_number === 'string');
    const variables = {
        orderId: reference_number,
        input: { lines },
    };
    const result = await client.mutate<CreateFulfillmentMutation, CreateFulfillmentMutationVariables>({
        mutation: fulfillmentMutation,
        variables,
    });
    if (result.data.orderFulfill.orderErrors.length > 0) {
        logger.info('Error-Result from Saleor', JSON.stringify(result.data.orderFulfill, null, 2), JSON.stringify(variables, null, 2));
        throw new Error(`Error creating fulfillment in Saleor '+ ${JSON.stringify(result.data.orderFulfill.orderErrors)} ${JSON.stringify(variables, null, 2)}`);
    } else {
        logger.info('Created Fulfillment in Saleor successful');
    }

    const fullfillId = result.data.orderFulfill.fulfillments[0].id;
    assert(typeof fullfillId === 'string');
    logger.info('Adding tracking number to fulfillment', fullfillId);

    // add the tracking data after creating the fulfillment
    const trackingResult = await client.mutate<SetTrackingNumberMutation, SetTrackingNumberMutationVariables>({
        mutation: updateTrackingMutation,
        variables: {
            id: fullfillId,
            trackingNumber: packagedata.tracking_number,
        },
    });
    if (trackingResult.data.orderFulfillmentUpdateTracking.orderErrors.length < 0) throw new Error('Adding tracking number not successful!');
    logger.info('Adding Fullfillment to Saleor Order successfully');

    return true;
};

/**
 * This function sets the needed package information as metadata for a mailchimp user and creates a fulfillment afterwards
 */
async function fulfillOrderInMailChimp(email :string, salesorderId :string, { tracking_number, carrier, tracking_portal_url }
:{ tracking_number:string, carrier: string, tracking_portal_url ?:string}, logger :winston.Logger) {
    const chimpId = callMailchimp.getMailchimpIdFromEmail(email);

    const storeData = await client.query<ShopInfoQuery, ShopInfoQueryVariables>({
        query: getShopInfo,
    });
    if (!storeData.data.shop) throw new Error('Did not receive valid result from Saleor to create/update store in MailChimp');
    const StoreID = await callMailchimp.SetupStore(storeData.data.shop);

    logger.info('Setting now Mailchimp merge fields for tracking number and carrier');
    // set the metadata / merge fields for the specific user
    await callMailchimp.UpdateUser(chimpId, {
        merge_fields: {
            MMERGE45: tracking_number,
            MMERGE46: carrier,
            MMERGE47: tracking_portal_url,
        },
    });

    const Update = {
        fulfillment_status: 'shipped',
    };

    logger.info('Updating the Mailchimp order with status fullfilled');
    await callMailchimp.UpdateOrder(StoreID, salesorderId, Update);
}

/**
 * Logic that works with an Zoho packages array.
 * @param rawPackages
 * @param salesorder the full salesorder used to get some more needed metadata.
 */
export const packagesLogic = async (rawPackages :Package[], salesorder :SalesOrder, logger:winston.Logger) => {
    // little constants helper / translation STORE-26 to just 26
    const zohoSalesOrderNumber = salesorder.salesorder_number;
    const MailchimpOrderId = returnMailchimpOrderId(zohoSalesOrderNumber);

    for (const pkg of rawPackages) {
        if (pkg.status === 'not_shipped') {
            logger.info('We have newly created packages in Zoho, that are not shipped yet. Checking, if we need to print stuff...');
            const packagedata = await getPackage({
                package_id: pkg.package_id,
            });
            // check in the custom field "label_printed"
            const labelPrintedAlready = packagedata?.custom_field_hash.cf_label_printed_unformatted;
            if (labelPrintedAlready) {
                logger.info('Label got already printed');
                continue;
            }

            const printResult = await printPackageSlip(pkg).catch((e) => {
                logger.error(e);
                Sentry.captureException(e);
            });
            // set the custom field "label_printed" to true
            if (printResult) {
                // const customFieldId = packagedata.custom_fields.find((field) => field.label === 'label_printed')?.customfield_id;
                // await setCheckboxValue('packages', pkg.package_id, customFieldId, true);
            }
        }

        if (pkg.status === ('shipped' || 'delivered')) {
            logger.info('Received a salesorder with newly shipped package(s)');

            // eslint-disable-next-line @typescript-eslint/naming-convention
            const { reference_number } = salesorder;
            // pull more data about this package from zoho to know all the line items and comments in this package
            const packagedata = await getPackage({
                package_id: pkg.package_id,
            });
                // check in the custom field "label_printed" - in the SHIPMENTORDER - not package
            const labelPrintedCf = packagedata?.shipmentorder_custom_fields?.find((field) => field.label === 'carrier_label_printed');
            const labelPrintedAlready = labelPrintedCf?.value || false;

            // print out the carrier label for a new package. If the "service" field is not set, we have a manual shipment
            // so no carrier label in the system, that we could print
            if (pkg.status === 'shipped' && pkg?.shipment_order?.shipment_id && !labelPrintedAlready && packagedata.service) {
                logger.info('We need to print out the carrier shipping label..');
                const shipmentId = pkg.shipment_order.shipment_id;
                try {
                    await printCarrierLabel(shipmentId);

                    // set the custom field 'carrier_label_printed' to true --
                    // does not Work! Can't edit an already shipped package..
                    // const cfId = labelPrintedCf.customfield_id;
                    // await setCheckboxValue('shipmentorders', shipmentId, cfId, true);
                } catch (error) {
                    Sentry.captureException(new Error(error));
                    logger.error(error);
                }

                logger.info(`Shipping Country Code of this package: ${salesorder?.shipping_address?.country_code}`);
                // Handling of Special case orders to Switzerland for example.
                if (['CH', 'NO'].includes(salesorder?.shipping_address?.country_code)) {
                    logger.info('Receiving a package that goes to a special non-EU country. ');
                    const fullSalesorder = await getSalesorderById(salesorder.salesorder_id);
                    await printCustomsDocuments(fullSalesorder);
                }
            }

            logger.info('Working now on package with Zoho ID', pkg.package_id);

            if (!packagedata.tracking_number) {
                logger.info('Package without tracking number found. We skip the creation in Saleor');
                continue;
            }

            // add the easypost tracker
            try {
                logger.info('Adding an easypost tracker now.');
                await addTracker({ trackingCode: packagedata.tracking_number, carrier: 'DPD' });
            } catch (error) {
                logger.error(error);
                Sentry.captureException(error);
            }

            let trackingPortalUrl = '';
            // add the tracking Portal URL back to the salesorder
            try {
                let { carrier } = packagedata;
                // little cleanup for DPD or DPD Germany etc.
                if (carrier.includes('DPD')) carrier = 'DPD';
                const { country } = packagedata.shipping_address;
                const countryCode = {
                    Deutschland: 'de' as CountryCode,
                    Österreich: 'at' as CountryCode,
                    Schweiz: 'ch' as CountryCode,
                    Spanien: 'es' as CountryCode,
                    Frankreich: 'fr' as CountryCode,
                    'Vereinigtes Königreich': 'uk' as CountryCode,
                    Belgien: 'be' as CountryCode,
                    Niederlande: 'nl' as CountryCode,
                };
                const url = generateTrackingPortalURL(carrier as Carrier, countryCode[country], packagedata.tracking_number, packagedata.shipping_address.zip);

                if (url) {
                    await addNote(packagedata.shipment_id, url);
                    trackingPortalUrl = url;
                }
            } catch (error) {
                logger.error(error);
                Sentry.captureException(error);
            }

            /**
             * Mailchimp is activated - fulfill the order there
             */
            if (mailchimpActive && packagedata?.tracking_number && packagedata?.status === 'shipped') {
                const contact = await getContactById(salesorder.customer_id);
                const { email } = contact;
                if (email) {
                    try {
                        const carrier = packagedata.carrier || salesorder.delivery_method;
                        await fulfillOrderInMailChimp(email, MailchimpOrderId,
                            { tracking_number: packagedata.tracking_number, carrier, tracking_portal_url: trackingPortalUrl }, logger);
                        logger.info('Marked this order as fulfilled in MailChimp');
                    } catch (error) {
                        if (error.statusCode === 404) {
                            logger.info('This order does not exist in Mailchimp. Can\'t fulfill. Order Number:', MailchimpOrderId);
                        } else {
                            logger.error('Error fulfilling order in Mailchimp!', error);
                            Sentry.captureException(new Error(error));
                        }
                    }
                } else {
                    logger.info('This Salesorder has no contact person email included. Can not update Mailchimp. Customer_ID:', salesorder.customer_id);
                }
            }

            // pull the order from saleor to get the line_item_ids for this package
            const FullOrderData = await client.query<GetOrderQuery, GetOrderQueryVariables>({
                query: getOrderQuery,
                variables: {
                    id: reference_number,
                },
            });
            const SaleorOrder = FullOrderData?.data?.order;

            if (SaleorOrder && SaleorOrder.status !== 'FULFILLED') {
                const result = await fulfillOrderInSaleor({ SaleorOrder, packagedata, reference_number }, logger);
                if (result) logger.info('Fulfilling the order in Saleor was successfull!');
            } else {
                logger.info('Order already completely fulfilled in Saleor or not found. Skipping');
            }
        }
    }

    return true;
};
