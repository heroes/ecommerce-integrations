/* eslint-disable prefer-destructuring */
/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable @typescript-eslint/naming-convention */
import axios, { AxiosResponse } from 'axios';
import axiosRetry from 'axios-retry';
import { strict as assert } from 'assert';
import dayjs from 'dayjs';
import isBetween from 'dayjs/plugin/isBetween';

import { ClientCredentials, ModuleOptions, AccessToken } from 'simple-oauth2';
import FormData from 'form-data';
import Timeout from 'await-timeout';
import retry from 'async-retry';
import { ContactPerson,
    Contact,
    ContactWithFullAddresses,
    Address, InvoiceSettings,
    ContactSettings, LineItem, Bundle, SalesOrder, Invoice, Package, LanguageCode, InvoiceOptional, AddressOptional, SalesOrderReturn } from './types';

dayjs.extend(isBetween);

type ZohoConfig = {
    zohoClientId: string,
    zohoClientSecret: string,
    zohoTokenHost?: string,
    zohoTokenPath?: string,
    zohoOrgId: string,
};
const zohoConfig = { } as ZohoConfig;
/**
 * Setup all needed configuration to connect to Zoho.
 * @param opts The settings object (mandatory and optional)
 */
export function createConfig(opts :ZohoConfig) {
    zohoConfig.zohoClientId = opts.zohoClientId;
    zohoConfig.zohoClientSecret = opts.zohoClientSecret;
    zohoConfig.zohoOrgId = opts.zohoOrgId;
    zohoConfig.zohoTokenHost = opts.zohoTokenHost || 'https://accounts.zoho.eu';
    zohoConfig.zohoTokenPath = opts.zohoTokenPath || '/oauth/v2/token';
    return zohoConfig;
}

const tokenConfig = {
    scope: 'ZohoInventory.FullAccess.all',
};

// Global access token object. Caches the token between function calls
let accessToken:AccessToken;

async function authenticate() {
    // authenticate to the Zoho API
    // try to re-use existing and valid tokens

    if (!zohoConfig.zohoClientId) throw new Error('No Zoho oAuth client found. Please add using the env variable ZOHO_CLIENT_ID');
    if (!zohoConfig.zohoClientSecret) throw new Error('No Zoho oAuth client-secret found. Please add using the env variable ZOHO_CLIENT_SECRET');
    if (!zohoConfig.zohoOrgId) throw new Error('No Zoho organization found. Please add using the env variable ZOHO_ORGANIZATION_ID');

    const config :ModuleOptions = {
        client: {
            id: zohoConfig.zohoClientId,
            secret: zohoConfig.zohoClientSecret,
        },
        auth: {
            tokenHost: zohoConfig.zohoTokenHost,
            tokenPath: zohoConfig.zohoTokenPath,
        },
        options: {
            authorizationMethod: 'body',
        },
    };

    const clientCredentials = new ClientCredentials(config);

    try {
        if (!accessToken) {
            accessToken = await clientCredentials.getToken(tokenConfig);
        }
        if (accessToken.expired(300)) {
            accessToken = await clientCredentials.getToken(tokenConfig);
        }
    } catch (error) {
        console.error('Error accessing Zoho', error);
    }
    return accessToken;
}

async function createInstance(retry = true) {
    const { ZOHO_COOKIES, ZOHO_TOKEN, ZOHO_ORGANIZATION_ID } = process.env;
    const options = {
        baseURL: 'https://inventory.zoho.eu/api/v1',
        timeout: 7000,
        params: { organization_id: ZOHO_ORGANIZATION_ID },
    };

    if (ZOHO_COOKIES) {
        const instanceWithCookies = axios.create({
            ...options,
            headers: {
                Cookie: ZOHO_COOKIES,
                'X-ZCSRF-TOKEN': ZOHO_TOKEN,
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36',
            },
        });
        if (retry) axiosRetry(instanceWithCookies, { retries: 2, retryDelay: (retryCount) => retryCount * 1000 });
        return instanceWithCookies;
    }
    const auth = await authenticate();
    const { access_token, token_type } = auth.token;
    const instance = axios.create({
        ...options,
        headers: {
            Authorization: `${token_type} ${access_token}`,
        },
    });
    if (retry) axiosRetry(instance, { retries: 2 });
    return instance;
}

type taxes = {
    'tax_specific_type' :string,
    'tax_percentage_formatted':string,
    'is_value_added': boolean,
    'is_editable': boolean,
    'tax_id': string,
    'deleted': boolean,
    'tax_type': string,
    'tax_percentage': number
}[];
/**
 * Get possible metadata for salesorders, like possible tax rates and the custom field ID for "ready to fulfill"
 * @returns { } the ID of the custom field "Ready to Fulfill" in Zoho. We use this to control,
 * if an order is ready to be send out.
 */
export async function salesOrderEditpage() {
    const instance = await createInstance();
    const result = await instance.get('/salesorders/editpage');
    assert.strictEqual(result.data.code, 0);

    const taxes = result.data.taxes.filter((x) => x.deleted === false) as taxes;
    const customFieldReadyToFulfill = result.data.custom_fields.find((x) => x.placeholder === 'cf_ready_to_fulfill')?.customfield_id as string;
    if (!customFieldReadyToFulfill) throw new Error('Custom Field "Ready to Fulfill" for Salesorders is not created yet!! Please fix ASAP');
    return { taxes, customFieldReadyToFulfill };
}
export async function customField() {
    const instance = await createInstance();
    const result = await instance.get('/contacts/editpage');
    const returnValue = result.data.custom_fields.filter((x) => x.label === 'saleor-id');
    if (!returnValue) throw new Error('no Contact custom field for saleor-id found. Please create it first.');
    return returnValue[0].customfield_id;
}


export async function contactPersonID(invoiceId :string) {
    const instance = await createInstance();
    const result = await instance({
        url: '/invoices/editpage',
        params: {
            invoice_id: invoiceId,
        },
    });
    assert.strictEqual(result.data.code, 0);
    return {
        contact_person_id: result.data.contact.primary_contact_id as string,
        email: result.data.contact.email_id as string,
    };
}
/**
 * Create a contact and return the ID of the contact, the billing_address, shipping_address and contact person ID
 * @param data
 */
export async function createContact(data) {
    const instance = await createInstance();
    data = `JSONString=${encodeURIComponent(JSON.stringify(data))}`;
    const result = await instance({
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        url: '/contacts',
        data,
    });
    assert.strictEqual(result.data.code, 0);
    return {
        contact_id: result.data.contact.contact_id,
        contact_person_id: result.data.contact.contact_persons[0].contact_person_id,
        billing_address_id: result.data.contact.billing_address.address_id || null,
        shipping_address_id: result.data.contact.shipping_address.address_id || null,
    };
}

export async function createContactPerson(contactId :string, contactPerson :ContactPerson) {
    const instance = await createInstance();
    if (!contactId) throw new Error(`contactId missing! Can't create the contact person ${contactPerson}`);
    const createData = {
        ...contactPerson,
        contact_id: contactId,
    };
    const data = `JSONString=${encodeURIComponent(JSON.stringify(createData))}`;
    const result = await instance({
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        url: '/contacts/contactpersons',
        data,
    });
    assert.strictEqual(result.data.code, 0);
    return result.data.contact_person.contact_person_id;
}

/**
 * Search in Zoho for a package with that Tracking Number. Returns null if no match
 * @param trackingNumber
 */
export async function getPackageByTrackingNumber(trackingNumber :string) {
    const instance = await createInstance();
    const result = await instance({
        url: '/packages',
        params: {
            tracking_number_contains: trackingNumber,
        },
    });
    assert.strictEqual(result.data.code, 0);
    const returnValue = result.data.packages;
    if (returnValue.length < 1) return null;
    // we just receive a certain package subset - pulling the full package data to return it.
    const packageId = returnValue[0].package_id;
    const fullPackage = await getPackage({ package_id: packageId });
    return fullPackage;
}

/**
 * Search for a user in Zoho. Filters results for just customers! No vendors
 * @param param0
 */
export async function getContact({ first_name, last_name, email, company_name }:
{first_name :string, last_name :string, email :string, company_name :string}) {
    const instance = await createInstance();
    const result = await instance({
        url: '/customers',
        params: {
            first_name_contains: first_name,
            last_name_contains: last_name,
            email_contains: email,
            company_name_contains: company_name,
            usestate: false,
            sort_order: 'A',
            status: 'active',
        },
    });
    assert.strictEqual(result.data.code, 0);
    const returnValue = result.data.contacts;
    if (returnValue.length < 1) return null;
    // zoho might give us "closely" matching email addresses, so we select the return value with the exact same email address.
    const exactMatch = returnValue.find((x) => x.email === email);
    return exactMatch?.contact_id;
}

export function getContactById(contactId :string, allAdresses? :boolean) :Promise<ContactWithFullAddresses>;
export function getContactById(contactId :string, allAdresses? :undefined|false) :Promise<Contact>;
/**
 * Get a contact / customer by its ID. Returns also an addresses array!
 * @param {string} contactId
 * @param {boolean} allAdresses if enabled (default) makes a second API call to get all addresses assoticated with this contact
 */
export async function getContactById(contactId :string, allAdresses: boolean|undefined = true) :Promise<Contact|ContactWithFullAddresses> {
    const instance = await createInstance();
    const result = await instance({
        url: `/contacts/${contactId}`,
    });
    assert.strictEqual(result?.data?.code, 0);
    const returnValue = result?.data?.contact;
    if (returnValue.language_code === '') returnValue.language_code = 'de';

    if (allAdresses) {
        const addressResult = await instance({
            url: `/contacts/${contactId}/address`,
        });
        assert.strictEqual(addressResult.data.code, 0);
        return {
            ...returnValue as Contact,
            addresses: addressResult.data.addresses as Address[],
        } as ContactWithFullAddresses;
    }
    return {
        ...returnValue as Contact,
    };
}

/**
 * Get invoice_items from up to 25 Individual Salesorder Ids. The corresponding invoice will have
 * these Salesorders attached to.
 * @param salesorderIds Array of Salesorder Ids
 * @param contactId Zoho Contact Id for this invoice
 */
export async function getInvoiceDataFromMultipleSalesOrders(salesorderIds :string[], contactId :string, isInclusiveTax: boolean) {
    const instance = await createInstance();
    const salesorderString = salesorderIds.join(',');
    const result = await instance({
        url: '/invoices/include/salesorderitems',
        params: {
            salesorder_ids: salesorderString,
            is_inclusive_tax: isInclusiveTax,
        },
    });
    assert.strictEqual(result.data.code, 0);
    const invoiceSettingsResult = await instance({
        url: '/invoices/editpage/fromcontacts',
        params: {
            contact_id: contactId,
        },
    });
    assert.strictEqual(invoiceSettingsResult.data.code, 0);
    const invoiceSettings :InvoiceSettings = invoiceSettingsResult.data.invoice_settings;
    const contact :ContactSettings = invoiceSettingsResult.data.contact;
    let lineItems :LineItem[] = result.data.invoice_items;
    lineItems = lineItems.map((item) => {
        delete item.warehouse_name;
        delete item.warehouses;
        return item;
    });
    const referenceNumber :string = result.data.reference_number;
    return { line_items: lineItems, reference_number: referenceNumber, invoice_settings: invoiceSettings, contact };
}

/**
 * This functions gets the total amount of all bundles of a specific month. Use this for example when you need to charge somebody
 * for the bundling and you need to count them per month.
 * @param from Configure the time range from parameter. Format: 2020-11-01
 * @param to Configure the time range to parameter.
 * @param userNameFilter Filter the created bundles by a specific Username
 */
export async function getBundles(from :string, to :string, userNameFilter? :string) {
    const instance = await createInstance();
    const responseData = await instance({
        url: '/bundles',
        method: 'get',
    });
    assert.strictEqual(responseData.data.code, 0);
    const From = dayjs(from);
    const To = dayjs(to);
    const bundles :Bundle[] = responseData.data.bundles;
    const bundlesWithDate = bundles.filter((bundle) => {
        const parsedDate = dayjs(bundle.date);
        let user = true;
        if (userNameFilter && bundle.created_by_name !== userNameFilter) user = false;
        return parsedDate.isBetween(From, To) && user;
    });
    const totalBundles = bundlesWithDate.reduce((previous, current) => previous + current.quantity_to_bundle, 0);
    return totalBundles;
}

/**
 * Add an address to a contact. Do not change the default address or edit any existing documents from this customer.
 * @param contactId
 * @param address
 */
export async function addAddresstoContact(contactId :string, address :AddressOptional, retries = 1) {
    const instance = await createInstance();
    const adressConstructor = { ...address };
    // we don't want to change the address at existing transactions in Zoho.
    adressConstructor.update_existing_transactions_address = 'false';
    let addressId = '';
    await retry(async () => {
        const data = `JSONString=${encodeURIComponent(JSON.stringify(adressConstructor))}`;
        const result = await instance({
            url: `/contacts/${contactId}/address`,
            method: 'post',
            data,
        });
        if (result?.data?.code !== 0) throw new Error(`Adding the address was not successfull! ${address}`);
        addressId = result.data.address_info.address_id as string;
    }, {
        retries,
    });
    return addressId;
}

/**
 * gets a salesorder by ID
 * @param salesorderId
 */
export async function getSalesorderById(salesorderId :string) {
    const instance = await createInstance();
    const result = await instance({
        url: `/salesorders/${salesorderId}`,
    });
    assert.strictEqual(result.data.code, 0);
    return result.data.salesorder as SalesOrderReturn;
}

interface SalesOrderWithInvoicedAmount extends SalesOrderReturn {
    total_invoiced_amount :number
}
/**
 * get a salesorder by salesorder number! Searches for the number first.
 * @param salesorderNumber
 */
export async function getSalesorder(salesorderNumber :String) {
    const instance = await createInstance();
    const result = await instance({
        url: '/salesorders',
        params: {
            salesorder_number: salesorderNumber,
        },
    });
    assert.strictEqual(result.data.code, 0);
    const returnValue = result.data.salesorders;
    if (returnValue.length < 1) return null;

    // get the full salesorder with metadata. Search results just offer parts
    const FullResult = await instance({
        url: `/salesorders/${returnValue[0].salesorder_id}`,
    });
    assert.strictEqual(FullResult.data.code, 0);

    // this easy to use value is NOT set by Zoho when accessing the Salesorder directly ..
    FullResult.data.salesorder.total_invoiced_amount = returnValue[0].total_invoiced_amount;

    return FullResult.data.salesorder as SalesOrderWithInvoicedAmount;
}

type invoiceStatus = 'paid' | 'void' | 'unpaid' | 'partially_paid';
/**
 * Get a list of invoices
 * @param filters
 */
export async function getInvoices({ date_before, status, customview_id } :{ date_before :string, status :invoiceStatus, customview_id? :string}) {
    const instance = await createInstance();
    const result = await instance({
        url: '/invoices',
        params: {
            date_before,
            status,
            customview_id,
        },
    });
    assert.strictEqual(result.data.code, 0);

    return result.data.invoices as Invoice[];
}

/**
 * gets an invoice by its Zoho Invoice ID
 * @param invoiceId
 */
export async function getInvoiceById(invoiceId :string) {
    const instance = await createInstance();
    const result = await instance({
        url: `/invoices/${invoiceId}`,
    });
    assert.strictEqual(result.data.code, 0);
    return result.data.invoice as Invoice;
}

/**
 * Get a corresponding invoice from a salesorder. Fails, if this Saleosorder has no, or more than one invoices attached.
 * @param salesOrderId The Zoho ID of the Salesorder
 */
export async function getInvoicefromSalesorder(salesOrderId :string) {
    const instance = await createInstance();
    const result = await instance({
        url: '/salesorders/editpage',
        params: {
            salesorder_id: salesOrderId,
        },
    });
    assert.strictEqual(result.data.code, 0);
    const returnValue = result.data.salesorder.invoices;
    if (returnValue.length < 1) throw new Error('This salesorder has no invoices attached.');
    if (returnValue.length > 1) throw new Error('This salesorder has more than one invoice attached. We can\'t add a payment');
    return {
        invoice_id: returnValue[0].invoice_id,
        metadata: returnValue[0],
    };
}
/**
 * Search for a product by SKU in Zoho and get the first result. Return the taxRate in percent and the Item ID
 * @param {object} param0
 * @param {string} param0.product_sku The SKU of the product to look for
 */
export async function getItembySKU({ product_sku }) {
    const instance = await createInstance();
    const result = await instance({
        url: '/items',
        params: {
            sku: product_sku,
        },
    });
    assert.strictEqual(result.data.code, 0);
    const returnValue = result.data.items;
    if (returnValue < 1) throw new Error(`No product for this SKU found! Create the product first in Zoho ${product_sku}`);
    return {
        zohoItemId: returnValue[0].item_id as string,
        zohoTaxRate: returnValue[0].tax_percentage as number,
        name: returnValue[0].name as string,
        zohoTaxRateId: returnValue[0].tax_id as string,
    };
}

/**
 * Get the Item from Zoho using its ID
 * @param {object} param0
 * @param {string} param0.product_id The Zoho product ID like 116240000000037177
 */
export async function getItem({ product_id }) {
    if (!product_id) throw new Error('Missing mandatory field product ID!');
    const instance = await createInstance();
    const result = await instance({
        url: `/items/${product_id}`,

    });
    assert.strictEqual(result.data.code, 0);
    if (!result.data.item) throw new Error(`No Product data returned from Zoho! ${JSON.stringify(result.data)}`);
    return result.data.item;
}
export async function getItemGroup(product_id) {
    const instance = await createInstance();
    const result = await instance({
        url: `/itemgroups/${product_id}`,

    });
    assert.strictEqual(result.data.code, 0);
    if (!result.data.item_group) throw new Error(`No Product data returned from Zoho! ${JSON.stringify(result.data)}`);
    return result.data.item_group;
}

/**
 * get the total amount of packages in a specific time-range. Date format: 2020-11-02
 * @param param0
 */
export async function getPackagesTotal({ from, to }:{ from :string, to :string }) {
    const instance = await createInstance();
    const result = await instance({
        url: '/packages/',
        headers: {
            'X-ZB-SOURCE': 'zbclient',
        },
        params: {
            shipment_date_start: from,
            shipment_date_end: to,
            response_option: 2,
        },

    });
    assert.strictEqual(result.data.code, 0);
    const totalAmount :number = result.data?.page_context?.total;
    return totalAmount;
}
export async function getPackage({ package_id }) {
    const instance = await createInstance();
    const result = await instance({
        url: `/packages/${package_id}`,
        headers: {
            'X-ZB-SOURCE': 'zbclient',
        },

    });
    assert.strictEqual(result.data.code, 0);
    return result.data.package as Package;
}
/**
 * Create an invoice in Zoho. Set totalGrossAmount to compare the result of the salesorder with a total Gross Amount.
 * @param rawData
 * @param totalGrossAmount
 */
export async function createInvoice(rawData :InvoiceOptional, totalGrossAmount? :number, timeout = 20000) {
    const instance = await createInstance();
    instance.defaults.timeout = timeout;
    const data = `JSONString=${encodeURIComponent(JSON.stringify(rawData))}`;

    const result = await instance({
        method: 'post',
        url: '/invoices',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        data,
    });

    assert.strictEqual(result.data.code, 0);
    return result.data.invoice.invoice_number as string;
}

/**
 * Creates a salesorder in Zoho. Set totalGrossAmount to compare the result of the salesorder with a total Gross Amount.
 * @param rawData
 * @param totalGrossAmount
 */
export async function createSalesorder(rawData :SalesOrder, totalGrossAmount? :string) {
    const instance = await createInstance(false);
    const total_gross_amount = parseFloat(totalGrossAmount);
    const data = `JSONString=${encodeURIComponent(JSON.stringify(rawData))}`;

    const result = await instance({
        method: 'post',
        url: '/salesorders',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        params: {
            ignore_auto_number_generation: true,
        },
        data,
    });
    if (result.data.code !== 0) {
        console.error('Creating the Salesorder failed in Zoho. Printing out response and the salesorder we wanted to create', result.data);
        console.info(JSON.stringify(data, null, 2));
    }
    assert.strictEqual(result.data.code, 0);
    if (total_gross_amount) assert.strictEqual(total_gross_amount, result.data.salesorder.total);

    return result.data.salesorder as SalesOrderReturn;
}

/**
 * Update specific values of a salesorder
 * @param id
 * @param rawData
 */
export async function updateSalesorder(id :string, rawData, retries = 0) {
    if (typeof id !== 'string') throw new Error('You are missing the salesorderid! Please set it');
    const instance = await createInstance();
    const data = `JSONString=${encodeURIComponent(JSON.stringify(rawData))}`;
    await retry(async () => {
        const result = await instance({
            method: 'put',
            url: `/salesorders/${id}`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            data,
        });
        assert.strictEqual(result.data.code, 0);
        return true;
    }, {
        retries,
    });
}

/**
 * Confirm several sales orders at once. Takes long for > 10 salesorders. Limit is 25 salesorders (I think)
 * @param salesorders Array of Salesorder Ids to confirm at once
 * @param retries the number of retries we should do when request fails
 */
export async function salesordersConfirm(salesorders :string[], retries = 3) {
    const instance = await createInstance();
    const data = `salesorder_ids=${encodeURIComponent(salesorders.join(','))}`;
    await retry(async () => {
        const result = await instance({
            method: 'post',
            url: '/salesorders/status/confirmed',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            data,
        });
        assert.strictEqual(result.data.code, 0);
        return true;
    }, {
        retries,
    });
}

export async function salesorderConfirm(salesorderId :string, retries = 3) {
    const instance = await createInstance();
    await retry(async () => {
        const result = await instance({
            method: 'post',
            url: `/salesorders/${salesorderId}/status/confirmed`,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
        });
        assert.strictEqual(result.data.code, 0);
    }, {
        retries,
    });
    return true;
}
/**
 * Takes an existing non-draft Salesorder and converts it to an invoice
 * @param {string} data Salesorder ID - the unique ID of this salesorder in Zoho
 */
export async function invoicefromSalesorder(data) {
    const instance = await createInstance();
    instance.defaults.timeout = 15000;
    const result = await instance({
        url: '/invoices/fromsalesorder',
        method: 'post',
        params: {
            salesorder_id: data,
        },
    });
    assert.strictEqual(result.data.code, 0);
    const returnValue = result.data.invoice;
    return returnValue.invoice_id;
}
export async function createPayment(paymentData) {
    const instance = await createInstance();
    const data = `JSONString=${encodeURIComponent(JSON.stringify(paymentData))}`;
    const result = await instance({
        url: '/customerpayments',
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        data,
    });
    assert.strictEqual(result.data.code, 0);
    return true;
}
/**
 * Update an invoice. Takes the full invoice_object, including the invoice_id as input parameter
 * @param invoiceID
 * @param updateData {}
 */
export async function updateInvoice(invoiceID :string, updateData :InvoiceOptional) {
    const instance = await createInstance();
    const data = `JSONString=${encodeURIComponent(JSON.stringify(updateData))}`;
    const result = await instance({
        url: `/invoices/${invoiceID}`,
        method: 'put',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        data,
    });
    assert.strictEqual(result.data.code, 0);
    return result.data.invoice as Invoice;
}
type TemplateName = 'Vorkasse' | 'Out_for_delivery' | 'Paid_Invoice';
type ZohoEmailTemplate = {
    'email_template_id' :string,
    'documents': [],
    'subject':string,
    'cc_me': boolean,
    'name':string,
    'placeholder':string,
    'is_default': boolean,
    'type_formatted':string,
    'type':string
};

/**
 * Get the needed data from a Zoho Emailing Template used to send out Emails to a customer
 * @param entity
 * @param id
 * @param templateName
 * @param languageCode
 */
export async function getEmailTemplateData(entity :entity, id :string, templateName :TemplateName, languageCode? :LanguageCode) {
    const instance = await createInstance();

    // first get the Templates
    const templatesResult = await instance({
        url: '/settings/emailtemplates',
    });
    // generate the template with language code
    const templateNameWithLang = `${templateName}_${languageCode.toLowerCase()}`;
    const template = templatesResult.data?.emailtemplates.find((x) => x.name === templateNameWithLang) as ZohoEmailTemplate;
    if (!template) throw new Error(`No template with the name ${templateNameWithLang} found!`);

    // get the email data like body, subject or the email address ID.
    const response = await instance({
        url: `/${entity}/${id}/email`,
        params: {
            email_template_id: template.email_template_id,
        },
    });
    const emailDataResult = response.data;

    const emailData = {
        from_address: emailDataResult.data?.from_address as string,
        subject: emailDataResult.data.subject as string,
        body: emailDataResult.data.body as string,
    };
    return emailData;
}

/**
 * Send out an E-Mail by searching for the corresponding Email Template first
 * @param entity
 * @param id
 * @param templateName
 * @param userEmail the Email Address where to send to
 * @param languageCode The user language. The function tries to find an email template with _de or _en as suffix
 */
export async function sendEmailWithTemplate(entity :entity, id :string, templateName :TemplateName, userEmail:string, languageCode? :LanguageCode) {
    const instance = await createInstance();

    // first get the Templates
    const templatesResult = await instance({
        url: '/settings/emailtemplates',
    });
    // generate the template with language code
    const templateNameWithLang = `${templateName}_${languageCode.toLowerCase()}`;
    const template = templatesResult.data?.emailtemplates.find((x) => x.name === templateNameWithLang) as ZohoEmailTemplate;
    if (!template) throw new Error(`No template with the name ${templateNameWithLang} found!`);

    // get the email data like body, subject or the email address ID.
    const response = await instance({
        url: `/${entity}/${id}/email`,
        params: {
            email_template_id: template.email_template_id,
        },
    });
    const emailDataResult = response.data;

    const emailData = {
        from_address: emailDataResult.data?.from_address,
        to_mail_ids: [`${userEmail}`],
        subject: emailDataResult.data.subject,
        body: emailDataResult.data.body,
    };

    const form = new FormData();
    form.append('file_name', emailDataResult.data?.file_name);
    form.append('JSONString', JSON.stringify(emailData));
    form.append('attach_pdf', 'true');
    // send out the email with the data from the step before
    const emailSendResult = await instance({
        url: `/${entity}/${id}/email`,
        headers: form.getHeaders(),
        method: 'post',
        data: form,
    });
    assert.strictEqual(emailSendResult.data.code, 0);
    return true;
}

export async function sendInvoice(data) {
    const instance = await createInstance();
    const result = await instance({
        url: `/invoices/${data.invoices[0].invoice_id}/email`,
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: `JSONString=${encodeURIComponent(JSON.stringify({
            send_from_org_email_id: true,
            to_mail_ids: [data.email],
        }))}`,
    });
    assert.strictEqual(result.data.code, 0);
}
/**
 * Get a public download-link with 90 days validity to directly download an invoice
 * @param {string} invoiceId The invoice ID we should create the download link for
 */
export async function getPublicInvoiceDownloadURL({ invoiceId, invoiceURL } :{ invoiceId? :string, invoiceURL? :string}) {
    const instance = await createInstance();
    const expiry_time = dayjs().add(90, 'day').format('YYYY-MM-DD');
    let returnValue;
    if (invoiceId) {
        const result = await instance({
            params: {
                transaction_id: invoiceId,
                transaction_type: 'invoice',
                link_type: 'public',
                expiry_time,
            },
        });
        assert.strictEqual(result.data.code, 0);
        returnValue = replace(result.data.share_link);
    } else {
        returnValue = replace(invoiceURL);
    }

    /**
     * Dangerously changing the URL link to be a direct download link. Zoho might change that in the future....
     * @param input
     */
    function replace(input) {
        return `${input.replace(/\/inventory/, '/books').replace(/\/secure/, '/api/v3/clientinvoices/secure').trim() as string}&accept=pdf`;
    }

    return returnValue;
}

type entity = 'salesorders' | 'packages' | 'invoices' | 'shipmentorders';
/**
 * Downloads a PDF package slip for a package and returns it as base64 string for further processing (like printing). Retries
 * when a document doesn't seem to exist
 * @param entity
 * @param id
 * @param additions add a string of additions to the download URL (like /image etc.)
 */
export async function getDocumentBase64StringOrBuffer(entity :entity, id :string, additions? :string) {
    const instance = await createInstance();
    const url = `/${entity}/${id}${additions || ''}`;

    let result :AxiosResponse;

    const RetryFunction = async () => {
        result = await instance({
            url,
            params: {
                accept: 'pdf',
            },
            responseType: 'arraybuffer',
        });
    };

    try {
        await RetryFunction();
    } catch (error) {
        if (error?.response?.status === (404 || 400)) {
            await Timeout.set(1000);
            await RetryFunction();
            console.error('Document still doesn\'t exist!');
            return { base64Buffer: null, filename: null, rawBuffer: null };
        }
        console.error('Error downloading document!', error);
        throw new Error(error);
    }

    const rawBuffer = Buffer.from(result.data, 'binary');
    const base64Buffer = Buffer.from(result.data, 'binary').toString('base64');
    const headerLine = result.headers['content-disposition'];
    const startFileNameIndex = headerLine.indexOf('"') + 1;
    const endFileNameIndex = headerLine.lastIndexOf('"');
    const filename = headerLine.substring(startFileNameIndex, endFileNameIndex) as string;

    return { base64Buffer, filename, rawBuffer };
}

/**
 * Adds a comment to an entity
 * @param entity
 * @param id
 * @param comment the comment string
 */
export async function addComment(entity :entity, id :string, comment :string) {
    const data = `JSONString=${encodeURIComponent(JSON.stringify({ description: comment }))}`;
    const instance = await createInstance();
    const result = await instance({
        url: `/${entity}/${id}/comments`,
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        data,
    });
    assert.strictEqual(result.data.code, 0);
    return true;
}

/**
 * Adds a note to a shipment
 * @param entity
 * @param id
 * @param comment the comment string
 */
export async function addNote(shipmentid :string, note :string) {
    const data = `shipment_notes=${encodeURIComponent(note)}`;
    const instance = await createInstance();
    const result = await instance({
        url: `/shipmentorders/${shipmentid}/notes`,
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        data,
    });
    assert.strictEqual(result.data.code, 0);
    return true;
}

export async function setCheckboxValue(entity :entity, id :string, customFieldId :string, value :boolean) {
    const instance = await createInstance();
    const updateValue = {
        custom_fields: [
            {
                customfield_id: customFieldId,
                value,
            },
        ],
    };
    const data = `JSONString=${encodeURIComponent(JSON.stringify(updateValue))}`;
    const result = await instance({
        method: 'put',
        url: `/${entity}/${id}`,
        data,
    });
    return true;
}
/**
 * returns a base 64 string and a filename for documents attached to a Salesorder
 * @param salesorderId
 * @param documentId
 */
export async function getDocumentFromSalesorderBase64String(salesorderId :string, documentId :string) {
    const instance = await createInstance();
    const result = await instance({
        url: `/salesorders/${salesorderId}/documents/${documentId}`,
        params: {
            accept: 'pdf',
        },
        responseType: 'arraybuffer',
    });
    const base64Buffer = Buffer.from(result.data, 'binary').toString('base64');
    const headerLine = result.headers['content-disposition'];
    const startFileNameIndex = headerLine.indexOf('"') + 1;
    const endFileNameIndex = headerLine.lastIndexOf('"');
    const filename = headerLine.substring(startFileNameIndex, endFileNameIndex);

    return { base64Buffer, filename };
}

/**
 *
 * @param {*} packageId
 * @param {*} salesorderId
 * @param {*} trackingNumber
 * @param {*} trackingRequired
 */
export async function createShipmentOrder(packageId, salesorderId, trackingNumber, trackingRequired = true) {
    const instance = await createInstance();
    const result = await instance({
        url: '/shipmentorders',
        method: 'post',
    });
}

// fetch("https://inventory.zoho.eu/api/v1/shipmentorders", {
//   "headers": {
//     "accept": "*/*",
//     "accept-language": "en-GB,en-US;q=0.9,en;q=0.8,de;q=0.7",
//     "cache-control": "no-cache",
//     "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
//     "pragma": "no-cache",
//     "sec-fetch-dest": "empty",
//     "sec-fetch-mode": "cors",
//     "sec-fetch-site": "same-origin",
//     "x-requested-with": "XMLHttpRequest",
//     "x-zb-asset-version": "Jul_01_2020_13903",
//     "x-zb-source": "zbclient",
//     "x-zcsrf-token": "zomcsparam=742850dcf7bce71793ba43fb24d6969bea3c9f902b68d70dec5bb013fd59ea6a983b950326f1596b057d6befafdf25f82c535363c63b4533cf615e3b79668813",
//     "x-zoho-include-formatted": "true"
//   },
//   "referrer": "https://inventory.zoho.eu/app",
//   "referrerPolicy": "no-referrer-when-downgrade",
//   "body": "package_ids=116240000000081279&salesorder_id=116240000000081144&is_delivered=false&send_notification=false&is_tracking_required=true&JSONString=%7B%22date%22%3A%222020-07-01%22%2C%22delivery_method%22%3A%22DPD%22%2C%22tracking_number%22%3A%2201906000679811%22%2C%22notification_email_ids%22%3A%5B%5D%2C%22notification_phone_numbers%22%3A%5B%5D%2C%22aftership_carrier_code%22%3A%22dpd%22%2C%22shipmentorder_custom_fields%22%3A%5B%5D%7D&organization_id=20070434578",
//   "method": "POST",
//   "mode": "cors",
//   "credentials": "include"
// });
