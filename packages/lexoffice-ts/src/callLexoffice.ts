import axios from 'axios';
import assert from 'assert';
import FormData from 'form-data';
import { LexofficeInvoiceObject, LexOfficeVoucher, VoucherStatus } from './types';

async function createInstance() {
    const token = process.env.LEXOFFICE_KEY;
    if (!token) throw new Error('Missing lexoffice API token! Set it via env variable LEXOFFICE_KEY');
    const options = {
        baseURL: 'https://api.lexoffice.io/v1',
        timeout: 6000,
    };

    return axios.create({
        ...options,
        headers: {
            Authorization: `Bearer ${token}`,
        },
    });
}

type voucherType = 'invoice' | 'salesinvoice';
/**
 * Get a voucher list from Lexoffice
 * @param voucherType
 * @param voucherStatus
 */
export const voucherlist = async (voucherType :voucherType, voucherStatus :VoucherStatus) => {
    const instance = await createInstance();
    const vouchers = await instance({
        url: '/voucherlist',
        params: {
            voucherType,
            voucherStatus,
        },
    });
    assert.strictEqual(vouchers.status, 200);

    return vouchers.data.content as [];
};

/**
 * Search for a voucher with a specific number
 * @param voucherNumber
 */
export const getVoucherByVoucherNumber = async (voucherNumber :string) => {
    const instance = await createInstance();
    const voucher = await instance({
        url: '/vouchers',
        params: {
            voucherNumber,
        },
    });
    assert.strictEqual(voucher.status, 200);
    return voucher.data?.content[0] as LexOfficeVoucher || null;
};

/**
 * Create a voucher in Lexoffice
 * @param voucherData
 * @returns lexOfficeInvoiceID
 */
export const createVoucher = async (voucherData :LexofficeInvoiceObject) => {
    const instance = await createInstance();
    const result = await instance({
        url: '/vouchers',
        method: 'post',
        data: voucherData,
    });
    return result.data.id as string;
};

/**
 * Uploads a file as multipart form-data to a voucher
 * @param filebuffer
 * @param filename
 */
export const addFiletoVoucher = async (filebuffer :Buffer, filename :string, voucherId :string) => {
    const instance = await createInstance();
    const form = new FormData();
    form.append('file', filebuffer, filename);
    await instance({
        url: `/vouchers/${voucherId}/files`,
        method: 'post',
        headers: form.getHeaders(),
        data: form,
    });

    return true;
};

/**
 * searches for the Email Adresse and returns the customer Id, if the customer does already exist.
 */
export const createContactIfnotExisting = async (
    { email, firstName, lastName, street, countryCode }
    :{email:string, firstName :string, lastName:string, street :string, countryCode :string},
) :Promise<string> => {
    const instance = await createInstance();

    const customerLookup = await instance({
        url: '/contacts',
        method: 'GET',
        params: {
            email,
            customer: true,
        },
    });
    if (customerLookup.data?.content?.length > 0) {
        console.log('Found a contact. Returning ID');
        return customerLookup.data.content[0].id;
    }
    const data = {
        version: 0,
        roles: {
            customer: {},
        },
        person: {
            firstName,
            lastName,
        },
        addresses: {
            billing: [
                {
                    street,
                    countryCode,
                },
            ],
        },
        emailAddresses: {
            business: [
                `${email}`,
            ],
        },
    };
    const createResult = await instance({
        url: '/contacts',
        method: 'post',
        data,
    });
    assert.strictEqual(createResult.status, 200);
    return createResult.data.id;
};
